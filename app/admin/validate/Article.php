<?php

namespace app\admin\validate;
use think\Validate;

class Article extends Validate
{
    protected $rule = [
        'title' => 'require',
        'cid' => 'require',
        'content' => 'require|token'
    ];

    protected $message = [
        'title.require' => '请输入标题',
        'cid.require' => '请选择分类',
        'content.require' => '请输入详细内容',
        'content.token' => '页面超时,请刷新网页重试',
    ];
}
