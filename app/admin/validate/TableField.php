<?php
namespace app\admin\validate;
use think\Validate;

class TableField extends Validate
{
    protected $rule = [
        'input'      => 'require',
        'sort_order' => 'require',
        'comment'    => 'requireIf:is_edit,1',
        'type'       => 'requireIf:is_edit,1|token',
    ];

    protected $message = [
        'input.require'      => '请选择输入框样式',
        'sort_order.require' => '排序不能为空',
        'field.requireIf'    => '字段名称不能为空',
        'comment.requireIf'  => '注释不能为空',
        'type.requireIf'     => '字段类型不能为空',
        'type.token'         => '页面失效,请刷新重试',
    ];
}
