<?php
namespace app\admin\validate;
use think\Validate;

class DictData extends Validate
{
    protected $rule = [
        'name' => 'require|token',
    ];

    protected $message = [
        'name.require' => '字典名称不能为空',
        'name.token' => '页面失效,请刷新重试',
    ];
}
