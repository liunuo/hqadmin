<?php

namespace app\admin\validate;
use think\Validate;

class Category extends Validate
{
    protected $rule = [
        'name' => 'require',
        'model' => 'require',
        'url' => 'requireIf:model,5|token',
    ];

    protected $message = [
        'name.require' => '分类名称不能为空',
        'model.require' => '模型不能为空',
        'url.requireIf' => '跳转链接不能为空',
        'url.token' => '页面超时,请刷新网页重试',
    ];
}
