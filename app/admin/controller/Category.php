<?php
namespace app\admin\controller;
use app\common\controller\AdminBase;
use app\common\model\Category as CategoryModel;

class Category extends AdminBase
{
    /**
     * 不验证权限
     * @var string[]
     */
    protected $noAuth = ['get_model_style'];

    /**
     * 栏目列表
     * @return mixed
     */
    public function index()
    {
        return $this->fetch('index');
    }
    public function index_json()
    {
        $list = CategoryModel::with(['model'])->order('sort_order desc,id desc')->select();
        $data = [];
        foreach ($list as $k=>$r){
            $data[$k]['authorityId'] = $r['id'];
            $data[$k]['id'] = $r['id'];
            $data[$k]['parentId'] = $r['pid'];
            $data[$k]['pid'] = $r['pid'];
            $data[$k]['name'] = $r['name'];
            $data[$k]['model_name'] = $r['model_name'];
            $data[$k]['authorityName'] = $r['name'];
            $data[$k]['status'] = $r['status'];
            $data[$k]['sort_order'] = $r['sort_order'];
            $data[$k]['is_tuijian'] = $r['is_tuijian'];
            $data[$k]['open'] = true;
        }
        $list = list_to_level($data);
        return json(['code'=>'0','msg'=>'','count'=>count($list),'data'=>$list]);
    }

    /**
     * 新增栏目
     * @return mixed
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $param = $this->request->param();
            $verify = input('_verify', true);
            //验证规则
            if($verify!='0'){
                try{
                    $this->validate($param, 'category');
                }catch (\Exception $e){
                    $this->error($e->getMessage());
                }
            }
            $result = CategoryModel::create($param);
            if ($result == true) {
                insert_admin_log('添加了分类');
                $this->success('添加成功');
            } else {
                $this->error($this->errorMsg);
            }
        }  
        return $this->fetch('save', ['pid' => CategoryModel::list_to_level()]);
    }

    /**
     * 编辑栏目
     * @param $id  栏目ID
     * @return mixed
     */
    public function edit($id)
    {
        if ($this->request->isPost()) {
            $param = $this->request->param();
            $verify = input('_verify', true);
            //验证规则
            if($verify!='0'){
                try{
                    $this->validate($param, 'category');
                }catch (\Exception $e){
                    $this->error($e->getMessage());
                }
            }
            //更新数据
            $resule = CategoryModel::update($param,['id'=>$param['id']]);
            if ( $resule == true) {
                insert_admin_log('修改了分类');
                $this->success('修改成功');
            } else {
                $this->error($this->errorMsg);
            }
        }
        $data  = CategoryModel::where('id',$id)->find();
        return $this->fetch('save', ['data' => $data,'pid' => CategoryModel::list_to_level()]);
    }

    /**
     * 删除栏目
     */
    public function del()
    {
        if ($this->request->isPost()) {
            $param = $this->request->param();
            $data = CategoryModel::where('pid',$param['id'])->count();
            if($data>'0'){
                $this->error('请先删除子栏目');
            }else{
                CategoryModel::destroy($param['id']);
                insert_admin_log('删除了分类');
                $this->success('删除成功');
            }
        }
    }

    
}
