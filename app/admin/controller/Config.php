<?php
namespace app\admin\controller;
use app\common\controller\AdminBase;
use app\common\model\System;

class Config extends AdminBase
{
    /**
     * 网站设置
     * @return mixed
     */
    public function setting()
    {
        if ($this->request->isPost()) {
            $param = $this->request->param();
            $param['photo'] = implode(',',array_filter($param['photo']));
            System::where('key','website')->update(['jdata'=>$param]);
            cache('website',null);
            $this->success('设置成功');
        }
        $data = System::where('key','website')->cache('website')->find()['jdata'];
        $data['photo'] = explode(',', $data['photo']);
        return $this->fetch('setting', ['data' => $data]);
    }

    /**
     * 参数配置
     * @return mixed
     */
    public function param()
    {
        if ($this->request->isPost()) {
            $param = $this->request->param();
            System::where('key',$param['model'])->update(['jdata'=>$param]);
            if($param['model'] == 'wxpay'){
                System::writeCertPemFiles($param['certpem'], $param['keypem']);
            }
            cache($param['model'],null);
            $this->success('设置成功');
        }
        $model = input('model')?:'storage';
        $data = System::where('key',$model)->cache($model)->find()['jdata'];
        return $this->fetch('param_'.$model, ['data' => $data]);
    }

    /**
     * 上传配置
     * @return mixed
     */
    public function upload()
    {
        if ($this->request->isPost()) {
            $param = $this->request->param();
            System::where('key','upload_setting')->update(['jdata'=>$param]);
            cache('upload_setting',null);
            insert_admin_log('修改了上传设置');
            $this->success('保存成功');
        }
        $data = System::where('key','upload_setting')->cache('upload_setting')->find()['jdata'];
        return $this->fetch('upload',['data' => $data]);
    }

}
