<?php
namespace app\admin\controller;
use app\common\controller\AdminBase;
use app\common\model\Zhuanti as Model;
use app\common\model\Category;

class Zhuanti extends AdminBase
{
    /**
     * 不验证权限
     * @var string[]
     */
    protected $noAuth = ['get_category'];

    /**
     * 专题列表
     * @return mixed
     */
    public function index()
    {
        return $this->fetch('index');
    }

    /**
     * 专题列表JSON
     * @param string $limit 分页
     * @param string $cid 栏目ID
     */
    public function index_json($limit='15',$cid='')
    {
        $qupu = new Model();
        if($cid){
            $qupu = $qupu->where('cid',$cid);
        }
        $list = $qupu->with(['category'])->order('sort_order desc,id desc')->paginate($limit);
        $this->result($list);
    }

    /**
     * 获取栏目树
     * @return \think\response\Json
     */
    public function get_category()
    {
        $list = Category::where('model',2)->order('sort_order desc,id desc')->select();
        foreach ($list as $k=>$r){
            $data[$k]['id'] = $r['id'];
            $data[$k]['parentId'] = $r['pid'];
            $data[$k]['pid'] = $r['pid'];
            $data[$k]['name'] = $r['name'];
            $data[$k]['organizationFullName'] = $r['name'];
            $data[$k]['sortNumber'] = $r['sort_order'];
            $data[$k]['sub_count'] = Category::where('pid',$r['id'])->where('status','1')->count();
        }
        $list = list_to_level($data);
        return json(['code'=>'0','msg'=>'','count'=>count($list),'data'=>$list]);
    }

    /**
     * 添加专题
     * @return mixed
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $param = $this->request->param();
            $param['user_id'] = '0';
            $param['status'] = '1';           
            //验证规则
            $verify = input('_verify', true);
            if($verify!='0'){
                try{
                    $this->validate($param, 'article');
                }catch (\Exception $e){
                    $this->error($e->getMessage());
                }
            }
            $result = Model::create($param);
            if ($result == true) {
                Model::update_url($result->id);
                insert_admin_log('添加了专题'.$param['title']);
                $this->success('添加成功', url('admin/zhuanti/index'));
            } else {
                $this->error($this->errorMsg);
            }
        }
        return $this->fetch('save', [
            'category'   => Category::list_to_level(2),
        ]);
    }

    /**
     * 编辑专题
     * @return mixed\
     */
    public function edit()
    {
        if ($this->request->isPost()) {
            $param  = $this->request->param();
            //验证规则
            $verify = input('_verify', true);
            if($verify!='0'){
                try{
                    $this->validate($param, 'article');
                }catch (\Exception $e){
                    $this->error($e->getMessage());
                }
            }
            //更新数据
            $resule = Model::update($param,['id'=>$param['id']]);
            if ( $resule == true) {
                Model::update_url($param['id']);
                insert_admin_log('修改了专题');
                $this->success('修改成功', url('admin/zhuanti/index'));
            } else {
                $this->error($this->errorMsg);
            }
        }
        return $this->fetch('save', [
            'data'      => Model::where('id', input('id'))->find(),
            'category'   => Category::list_to_level(2),
        ]);
    }

    /**
     * 删除专题
     */
    public function del()
    {
        if ($this->request->isPost()) {
            $param = $this->request->param();
            Model::destroy($param['id']);
            insert_admin_log('删除了专题');
            $this->success('删除成功');
            
        }
    }
    
}
