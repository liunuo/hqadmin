<?php
namespace app\admin\controller;
use app\common\controller\AdminBase;
use app\common\model\Table as Model;
use app\common\model\TableField;
use think\facade\Db;

class Table extends AdminBase
{
    /**
     * 字典列表
     * @return mixed
     */
    public function index()
    {
        return $this->fetch('index');
    }
    public function index_json($limit='15',$table_id)
    {
        $dict = new TableField();
        if(!empty($table_id)){
            $dict = $dict->where('table_id',$table_id);
        }
        $list = $dict->order('sort_order desc,id desc')->paginate($limit);
        $this->result($list);
    }

    /**
     * 获取字典分类树
     * @return \think\response\Json
     */
    public function get_table()
    {
        $dict = new Model();
        $list = $dict->order('id desc')->select();
        $data = [];
        foreach ($list as $k=>$r){
            $data[$k]['leftid'] = $k;
            $data[$k]['table_id'] = $r['id'];
            $data[$k]['table_name'] = $r['name'];
            $data[$k]['dictTable'] = $r['table'];
        }
        return json(['code'=>'0','msg'=>'','count'=>count($list),'data'=>$data]);
    }

    /**
     * 添加字典分类
     * @return mixed
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $param = $this->request->param();
            $result = Model::create($param);
            if ($result == true) {
                insert_admin_log('添加了表');
                $this->success('添加成功');
            } else {
                $this->error($this->errorMsg);
            }
        }
        return $this->fetch('save');
    }

    /**
     * 编辑字典分类
     * @return mixed
     */
    public function edit()
    {
        if ($this->request->isPost()) {
            $param  = $this->request->param();
            //更新数据
            $resule = Model::update($param,['id'=>$param['id']]);
            if ( $resule == true) {
                insert_admin_log('修改了');
                $this->success('修改成功');
            } else {
                $this->error($this->errorMsg);
            }
        }
        $data = Model::where('id', input('id'))->find();
        return $this->fetch('save',compact('data'));
    }

    /**
     * 删除字典分类
     */
    public function del()
    {
        if ($this->request->isPost()) {
            $param = $this->request->param();
            $data = TableField::with('tables')->where('table_id',$param['id'])->find();
            if(!empty($data)){
                $this->error('非空表禁止删除');
            }else{
                Model::destroy($param['id']);
                $this->success('删除成功');
            }
            
        }
    }

    /**
     * 更新字段列表
     */
    public function update_field()
    {
        if ($this->request->isPost()) {
            $param = $this->request->param();
            $result = Model::update_field($param['table_id']);
            if($result == true){
                $this->success('更新成功');
            }else{
                $this->error('更新失败');
            }
        }
    }
    /**
     * 添加字典数据
     * @return mixed
     */
    public function add_data()
    {
        if ($this->request->isPost()) {
            $param = $this->request->param();
            $verify = input('_verify', true);
            //验证规则
            if(!empty($verify)){
                try{
                    $this->validate($param, 'tableField');
                }catch (\Exception $e){
                    $this->error($e->getMessage());
                }
            }
            $result = TableField::create($param);
            if ($result == true) {
                $this->success('添加成功');
            } else {
                $this->error($this->errorMsg);
            }
        }
        $type = TableField::field_type();
        $input = TableField::field_input();
        return $this->fetch('save_data',compact('type','input'));
    }

    /**
     * 编辑字典数据
     * @param $id 字典ID
     * @return mixed
     */
    public function edit_data($id)
    {
        if ($this->request->isPost()) {
            $param  = $this->request->param();
            $verify = input('_verify', true);
            //验证规则
            if(!empty($verify)){
                try{
                    $this->validate($param, 'tableField');
                }catch (\Exception $e){
                    $this->error($e->getMessage());
                }
            }
            //更新数据
            if($param['is_edit'] == '1'){
                $result = TableField::update($param,['id'=>$param['id']]);
            }else{
                $result = TableField::update(['input'=>$param['input'],'sort_order'=>$param['sort_order']],['id'=>$param['id']]);
            }
            if ($result == true) {
                insert_admin_log('修改了字段');
                $this->success('修改成功');
            } else {
                $this->error($this->errorMsg);
            }
        }
        $type = TableField::field_type();
        $input = TableField::field_input();
        $data = TableField::find($id);
        return $this->fetch('save_data',compact('data','type','input'));
    }

    /**
     * 删除字典数据
     */
    public function del_data()
    {
        if ($this->request->isPost()) {
            $param = $this->request->param();
            $data = TableField::where('id',$param['id'])->find();
            if(isset($data['is_system']) && $data['is_system'] == 1){
                $this->error('系统字段禁止删除');
            }
            TableField::destroy($param['id']);
            $this->success('删除成功');
        }
    }

    public function a()
    {
        $info['name'] = 'abcdefg';
        $info['type'] = 'int';
        $info['length'] = '11';
        $info['isNull'] = '';
        $info['default'] = '0';
        $info['comment'] = '测试';
        TableField::addField('hq_achievements_hengxiang',$info);
        echo "1";
    }
}
