<?php
namespace app\admin\controller;
date_default_timezone_set("Asia/Chongqing");
error_reporting(E_ERROR);
header("Content-Type: text/html; charset=utf-8");
use app\common\controller\AdminBase;
use think\facade\Filesystem;
use think\facade\Config;
use app\common\model\Uploads as UploadsModel;

class Uploads extends AdminBase
{
    /**
     * 不验证权限
     * @var string[]
     */
    protected $noAuth = [ 'uploadImage', 'uploadFile', 'uploadVideo', 'editor'];

    /**
     * 上传附件列表
     * @return mixed
     */
    public function index()
    {
        return $this->fetch('index');
    }
    public function index_json($limit='15')
    {
        $list = UploadsModel::order('id desc')->paginate($limit);
        $this->result($list);
    }

    /**
     * 上传图片
     * @return array
     */
    public function uploadImage()
    {
        try {
            $file = request()->file('file');
            //处理图片
            UploadsModel::UploadValidate($file);
            $params = get_system('storage');
            //判断上传位置
            if($params['type'] == 'qiniu'){//七牛
                $key = date('Y/md/His_').substr(microtime(), 2, 6).'_'.mt_rand(0,999).'.'.$file->getOriginalExtension();
                $qiniu = new \app\common\library\Qiniu();
                $url = $params['domain'].$qiniu->upload($file->getRealPath(), $key);
                UploadsModel::CreateInfoAdmin('qiniu',$params['domain'], $key, $file->getSize(), $file->getOriginalMime());
                insert_admin_log('上传了图片');
                return ['code' => 1, 'url' => $url,'msg'=>'上传成功'];
            }else{//默认本地
                $savename = Filesystem::disk('public')->putFile('uploads',$file);
                $url = request()->domain().'/'.$savename;
                UploadsModel::CreateInfoAdmin('local','', $savename, $file->getSize(), $file->getOriginalMime());
                insert_admin_log('上传了图片');
                return ['code' => 1, 'url' => $url,'msg'=>'上传成功'];
            }
        } catch (\Exception $e) {
            return ['code' => 0, 'msg' => $e->getMessage()];
        }
    }

    /**
     * 删除图片
     */
    public function del()
    {
        if ($this->request->isPost()) {
            $param = $this->request->param();
            $files = UploadsModel::where('id',$param['id'])->find();
            //判断存储位置
            if($files->getData('storage')=='qiniu'){
                $key = $files['file_name'];
                $qiniu = new \app\common\library\Qiniu();
                $qiniu->delete($key);
                UploadsModel::destroy($param['id']);
                insert_admin_log('删除了图片');
                $this->success('删除成功');
            }elseif($files->getData('storage')=='local'){
                $result = unlink($files['file_name']);
                if($result){
                    $delete = UploadsModel::destroy($param['id']);
                    insert_admin_log('删除了图片');
                    $this->success('删除成功');
                }else{
                    $delete = UploadsModel::destroy($param['id']);
                    UploadsModel::where('id',$param['id'])->update(['status'=>'0']);
                    $this->success('删除失败');
                }
            }
            
        }
    }

    /**
     * 编辑器上传图片
     * @return array|\think\response\Json
     */
    public function editor_upload()
    {
        try {
            $file = request()->file('file');
            //处理图片
            UploadsModel::UploadValidate($file);
            $params = get_system('storage');
            //判断上传位置
            if($params['type']=='qiniu'){//七牛
                $key = date('Y/md/His_').substr(microtime(), 2, 6).'_'.mt_rand(0,999).'.'.$file->getOriginalExtension();
                $qiniu = new \app\common\library\Qiniu();
                $url = $params['domain'].$qiniu->upload($file->getRealPath(), $key);
                UploadsModel::CreateInfoAdmin('qiniu',$params['domain'], $key, $file->getSize(), $file->getOriginalMime());
                insert_admin_log('上传了图片');
                return json(['location'=>$url]);
            }else{//默认本地
                $savename = Filesystem::disk('public')->putFile('uploads',$file);
                $url = request()->domain().'/'.$savename;
                UploadsModel::CreateInfoAdmin('local','', $savename, $file->getSize(), $file->getOriginalMime());
                insert_admin_log('上传了图片');
                return json(['location'=>$url]);
            }
        } catch (\Exception $e) {
            return ['code' => 0, 'msg' => $e->getMessage()];
        }
    }

    
}
