<?php
namespace app\admin\controller;
use app\common\controller\AdminBase;
use app\common\model\Dict as DictModel;
use app\common\model\DictData;

class Dict extends AdminBase
{
    /**
     * 字典列表
     * @return mixed
     */
    public function index()
    {
        return $this->fetch('index');
    }
    public function index_json($limit='15',$dict_id)
    {
        $dict = new DictData();
        if($dict_id){
            $dict = $dict->where('dict_id',$dict_id);
        }
        $list = $dict->order('sort_order desc,id desc')->paginate($limit);
        $this->result($list);
    }

    /**
     * 获取字典分类树
     * @return \think\response\Json
     */
    public function get_dictionary()
    {
        $dict = new DictModel();
        $list = $dict->order('id desc')->select();
        $data = [];
        foreach ($list as $k=>$r){
            $data[$k]['leftid'] = $k;
            $data[$k]['dictId'] = $r['id'];
            $data[$k]['dictName'] = $r['name'];
        }
        return json(['code'=>'0','msg'=>'','count'=>count($list),'data'=>$data]);
    }

    /**
     * 添加字典分类
     * @return mixed
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $param = $this->request->param();
            $result = DictModel::create($param);
            if ($result == true) {
                insert_admin_log('添加了字典分类');
                $this->success('添加成功');
            } else {
                $this->error($this->errorMsg);
            }
        }
        return $this->fetch('save');
    }

    /**
     * 编辑字典分类
     * @return mixed
     */
    public function edit()
    {
        if ($this->request->isPost()) {
            $param  = $this->request->param();
            //更新数据
            $resule = DictModel::update($param,['id'=>$param['id']]);
            if ( $resule == true) {
                insert_admin_log('修改了字典类型');
                $this->success('修改成功');
            } else {
                $this->error($this->errorMsg);
            }
        }
        $data = DictModel::where('id', input('id'))->find();
        return $this->fetch('save',compact('data'));
    }

    /**
     * 删除字典分类
     */
    public function del()
    {
        if ($this->request->isPost()) {
            $param = $this->request->param();
            $data = DictData::with('dict')->where('dict_id',$param['id'])->find();
            if($data['dict']['is_system'] == 1){
                $this->error('系统字典,禁止删除');
            }
            if(!empty($data)){
                $this->error('请先删除字典数据');
            }else{
                DictModel::destroy($param['id']);
                insert_admin_log('删除了字典类型');
                $this->success('删除成功');
            }
            
        }
    }

    /**
     * 添加字典数据
     * @return mixed
     */
    public function add_data()
    {
        if ($this->request->isPost()) {
            $param = $this->request->param();
            $verify = input('_verify', true);
            //验证规则
            if(!empty($verify)){
                try{
                    $this->validate($param, 'dictData');
                }catch (\Exception $e){
                    $this->error($e->getMessage());
                }
            }
            $result = DictData::create($param);
            if ($result == true) {
                insert_admin_log('添加了字典');
                $this->success('添加成功');
            } else {
                $this->error($this->errorMsg);
            }
        }
        return $this->fetch('save_data');
    }

    /**
     * 编辑字典数据
     * @param $id 字典ID
     * @return mixed
     */
    public function edit_data($id)
    {
        if ($this->request->isPost()) {
            $param  = $this->request->param();
            $verify = input('_verify', true);
            //验证规则
            if(!empty($verify)){
                try{
                    $this->validate($param, 'dictData');
                }catch (\Exception $e){
                    $this->error($e->getMessage());
                }
            }
            //更新数据
            $resule = DictData::update($param,['id'=>$param['id']]);
            if ( $resule == true) {
                insert_admin_log('修改了字典名称');
                $this->success('修改成功');
            } else {
                $this->error($this->errorMsg);
            }
        }
        return $this->fetch('save_data', [
            'data'  => DictData::where('id',$id)->find(),
        ]);
    }

    /**
     * 删除字典数据
     */
    public function del_data()
    {
        if ($this->request->isPost()) {
            $param = $this->request->param();
            $data = DictData::where('dict_id',$param['id'])->find();
            if($data['is_system'] == 1){
                $this->error('系统字段禁止删除');
            }
            DictData::destroy($param['id']);
            insert_admin_log('删除了字典名称');
            $this->success('删除成功');
            
        }
    }

}
