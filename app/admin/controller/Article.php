<?php
namespace app\admin\controller;
use app\common\controller\AdminBase;
use app\common\model\Article as ArticleModel;
use app\common\model\Category;
use app\common\model\Page;

class Article extends AdminBase
{
    /**
     * 文章列表
     * @return mixed
     */
    public function index($form='article')
    {
        $category = Category::list_to_level($form);
        return $this->fetch('index_'.$form,compact('category','form'));
    }

    /**
     * 文章列表JSON
     * @param string $limit 分页码
     * @param string $cid 栏目ID
     */
    public function index_json($limit='15',$form='article')
    {
        $model = $this->get_model($form);
        $param = $this->request->param();
        if(!empty($param['keyword'])){
            $model = $model->where('title','like','%'.$param['keyword'].'%');
        }
        if(!empty($param['cid'])){
            $model = $model->where('cid',$param['cid']);
        }
        $list = $model->with(['category'])->field('id,cid,title,create_time,sort_order,is_hot,is_tuijian,status')->order('sort_order desc,id desc')->paginate($limit);
        $this->result($list);
    }

    /**
     * 新增文章
     * @return mixed
     */
    public function add($form='article')
    {
        if ($this->request->isPost()) {
            $param = $this->request->param();
            $param['user_id'] = session('admin_auth.admin_id');
            $param['status'] = '1';
            //验证规则
            $verify = input('_verify', true);
            if($verify!='0'){
                try{
                    $this->validate($param, 'article');
                }catch (\Exception $e){
                    $this->error($e->getMessage());
                }
            }
            $model = $this->get_model($form);
            $result = $model->create($param);
            if ($result == true) {
                insert_admin_log('添加了文章'.$param['title']);
                $this->success('添加成功', url('admin/article/index',['form'=>$form]));
            } else {
                $this->error($this->errorMsg);
            }
        }
        return $this->fetch('save_'.$form, [
            'category'   => Category::list_to_level($form),
        ]);
    }

    /**
     * 编辑文章
     * @return mixed
     */
    public function edit($form='article')
    {
        $model = $this->get_model($form);
        if ($this->request->isPost()) {
            $param  = $this->request->param();
            //验证规则
            $verify = input('_verify', true);
            if($verify!='0'){
                try{
                    $this->validate($param, 'article');
                }catch (\Exception $e){
                    $this->error($e->getMessage());
                }
            }
            //更新数据
            $resule = $model->update($param,['id'=>$param['id']]);
            if ( $resule == true) {
                insert_admin_log('修改了文章');
                $this->success('修改成功', url('admin/article/index',['form'=>$form]));
            } else {
                $this->error($this->errorMsg);
            }
        }
        return $this->fetch('save_'.$form, [
            'data'      => $model->where('id', input('id'))->find(),
            'category'   => Category::list_to_level($form),
        ]);
    }

    /**
     * 删除文章
     */
    public function del($form='article')
    {
        if ($this->request->isPost()) {
            $param = $this->request->param();
            $model = $this->get_model($form);
            $model->destroy($param['id']);
            insert_admin_log('删除了文章');
            $this->success('删除成功');
            
        }
    }

    /**
     * 读取FORM
     * @param $form
     */
    public function get_model($form)
    {
        if($form == 'article'){
            $model = new ArticleModel();
        }elseif ($form == 'page'){
            $model = new Page();
        }

        return $model;
    }
}
