<?php
namespace app\common\model;
use think\Model;
use think\facade\Db;

class User extends Model
{
    use \think\model\concern\SoftDelete;
    protected $deleteTime = 'delete_time';
    protected $defaultSoftDelete = 0;

    //保存省
    public function setProvinceAttr($value,$data)
    {
        if(isset($data['shengshi']) and $data['shengshi']!=''){
            $list = explode(',',$data['shengshi']);
            return $list[0];
        }else{
            return '';
        }
    }
    //保存城市
    public function setCityAttr($value,$data)
    {
        if(isset($data['shengshi']) and $data['shengshi']!=''){
            $list = explode(',',$data['shengshi']);
            return $list[1];
        }else{
            return '';
        }
    }
    //保存区县
    public function setRegionAttr($value,$data)
    {
        if(isset($data['shengshi']) and $data['shengshi']!=''){
            $list = explode(',',$data['shengshi']);
            return $list[2];
        }else{
            return '';
        }
    }

    
}