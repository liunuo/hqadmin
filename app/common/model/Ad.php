<?php
namespace app\common\model;
use think\Model;
use think\model\concern\SoftDelete;

class Ad extends Model
{
    use SoftDelete;
    protected $deleteTime = 'delete_time';
    protected $defaultSoftDelete = 0;

    /**
     * 关联分类
     * @return \think\model\relation\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(DictData::class, 'category', 'id')->bind(['category_name'=>'name']);
    }
}