<?php
namespace app\common\model;
use think\Model;

class Admin extends Model
{
    protected $autoWriteTimestamp = true;

    /**
     * MD5保存密码
     * @param $value
     * @return string
     */
    public function setPasswordAttr($value)
    {
        return md5($value);
    }

    /**
     * 关联获取分组
     * @return \think\model\relation\BelongsTo
     */
    public function authGroupAccess()
    {
        return $this->belongsTo(AuthGroupAccess::class, 'id', 'uid')->bind(['group_id']);
    }

    /**
     * 关联分组名称
     * @return \think\model\relation\BelongsTo
     */
    public function authGroup()
    {
        return $this->belongsTo(AuthGroup::class, 'group_id', 'id')->bind(['groupname'=>'name']);
    }

    /**
     * 获取上次登录时间,时间戳转时间
     * @param $value
     * @return false|string
     */
    public function getLastLoginTimeAttr($value)
    {
        return date('Y-m-d H:i:s',$value);
    }
}