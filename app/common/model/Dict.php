<?php
namespace app\common\model;
use think\Model;
use think\facade\Db;

class Dict extends Model
{
    /**
     * 关联权限
     * @return \think\model\relation\BelongsTo
     */
    public function userrule()
    {
        return $this->belongsTo(UserAuthRule::class, 'auth_id', 'id')->bind(['auth_name'=>'name']);
    }
    
}