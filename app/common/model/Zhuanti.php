<?php
namespace app\common\model;
use think\Model;
use think\facade\Db;
use think\model\concern\SoftDelete;

class Zhuanti extends Model
{
    use SoftDelete;
    protected $deleteTime = 'delete_time';
    protected $defaultSoftDelete = 0;
    protected $autoWriteTimestamp = true;

    /**
     * 生成URL
     * @param $id 文章ID
     */
    static function update_url($id) {
        $url = '/show/'.$id.'.html';
        $wxapp_url = "/pages/zhuanti/article?id=".$id;
        self::where(['id'=>$id])->update(['url'=>$url,'wxapp_url'=>$wxapp_url]);
        return;
    }

    static function list_to_level() {
        $list = list_to_level(self::order('id desc')->select());
        return $list;
    }
    //URL

    static function lists()
    {
        return self::where('status','1')->select();
    }

    /**
     * 关联栏目
     * @return \think\model\relation\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class, 'cid', 'id')->bind(['category_name'=>'name','template_show']);
    }

    static function images($data)
    {
        $ret = <<<EOT
            <div class="layui-upload-drag ajax-admin-images" id="logo">
                <div class=" upload-name">
                    <i class="layui-icon"></i>
                    <p>点击或将文件拖拽到此处</p>
                </div>
                <div class="upload-image">
                    <img src="" alt="上传成功后渲染" style="max-width: 196px">
                    <input type="hidden" name="logo" value="" autocomplete="off" placeholder="" class="layui-input">
                </div>
                <div class="layui-progress layui-progress-big upload-progress layui-hide" lay-showpercent="yes" lay-filter="logo">
                    <div class="layui-progress-bar" lay-percent=""></div>
                </div>
            </div>
        EOT;
        return $ret;
    }

    
}