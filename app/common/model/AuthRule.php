<?php

namespace app\common\model;
use think\facade\Session;
use think\Model;

class AuthRule extends Model
{
    // 获取导航栏
    public static function getNavbar()
    {
        $where = ['type' => 'nav', 'status' => 1];
        if (Session::get('admin_auth.admin_id') != '1') {
            $access  = AuthGroupAccess::with('authGroup')->where('uid', Session::get('admin_auth.admin_id'))->find();
            //$where['id'] = ['in', $access['rules']];
            if($access){
                $where = "type='nav' and status = '1' and id in(".$access['rules'].")";
            }
        }
        $navs = AuthRule::where($where)->order('sort_order asc')->select();
        return collection($navs)->toArray();
    }
}