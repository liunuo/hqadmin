<?php

namespace app\common\model;

use think\Model;

class AuthGroupAccess extends Model
{
    /**
     * 关联所属分组权限
     * @return \think\model\relation\BelongsTo
     */
    public function authGroup()
    {
        return $this->belongsTo(AuthGroup::class, 'group_id', 'id')->bind(['rules']);
    }
}