/*
 Navicat Premium Data Transfer

 Source Server         : 腾讯基础版
 Source Server Type    : MySQL
 Source Server Version : 50718
 Source Host           : mysql-1.ennn.cn:10130
 Source Schema         : admin_ennn_cn

 Target Server Type    : MySQL
 Target Server Version : 50718
 File Encoding         : 65001

 Date: 20/07/2021 10:52:11
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for hq_ad
-- ----------------------------
DROP TABLE IF EXISTS `hq_ad`;
CREATE TABLE `hq_ad`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `userid` smallint(8) NULL DEFAULT NULL COMMENT '用户ID',
  `category` smallint(5) NOT NULL DEFAULT 0 COMMENT '分类',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '分类名称',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '描述',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '链接',
  `target` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '打开方式',
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图片',
  `sort_order` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '排序',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '状态',
  `create_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(10) NOT NULL DEFAULT 0 COMMENT '更新时间',
  `delete_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '广告' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of hq_ad
-- ----------------------------
INSERT INTO `hq_ad` VALUES (5, NULL, 33, '发是打发阿什顿发', '', '', '_self', 'https://mdf.muxue.com.cn/uploads/20200814/6b57516592b5933b36dd3b601fd95540.png', 0, 1, 1591088366, 1597378767, 0);
INSERT INTO `hq_ad` VALUES (6, NULL, 33, '发是打发阿什顿发', '', '', '', 'http://admin.ennn.cn/uploads/20210406/749ff2e6452ca55dfc0913e62cb32bab.jpg', 0, 1, 1597322681, 1617676064, 0);
INSERT INTO `hq_ad` VALUES (7, NULL, 33, '', '', '', '', 'http://admin.ennn.cn/uploads/20210421/5f02225c7d94cf534b1d8aac5c0deb57.jpg', 0, 1, 1597322693, 1619015073, 0);

-- ----------------------------
-- Table structure for hq_admin
-- ----------------------------
DROP TABLE IF EXISTS `hq_admin`;
CREATE TABLE `hq_admin`  (
  `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '管理员用户名',
  `avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '头像',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `password` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '管理员密码',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '0禁用/1启动',
  `last_login_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '上次登录时间',
  `last_login_ip` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '上次登录IP',
  `login_count` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '登录次数',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `auth` tinyint(5) NULL DEFAULT 1 COMMENT '访问权限  1全部访问 0部分访问',
  `create_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '管理员' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of hq_admin
-- ----------------------------
INSERT INTO `hq_admin` VALUES (1, 'admin', NULL, '黄青', 'e10adc3949ba59abbe56e057f20f883e', 1, 1622644085, '220.200.62.15', 1427, 'd', 1, 1555249039, 1622644085);

-- ----------------------------
-- Table structure for hq_admin_log
-- ----------------------------
DROP TABLE IF EXISTS `hq_admin_log`;
CREATE TABLE `hq_admin_log`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `admin_id` smallint(5) UNSIGNED NOT NULL DEFAULT 0 COMMENT '管理员id',
  `username` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '管理员用户名',
  `useragent` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'User-Agent',
  `ip` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'ip地址',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '请求链接',
  `method` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '请求类型',
  `type` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '资源类型',
  `param` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '请求参数',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '日志备注',
  `create_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1834 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '管理员日志' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of hq_admin_log
-- ----------------------------
INSERT INTO `hq_admin_log` VALUES (1722, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '59.61.174.14', 'http://admin.ennn.cn/admin/auth/editRule.html?id=92', 'POST', 'json', '{\"id\":\"92\",\"pid\":\"4\",\"name\":\"\\u53c2\\u6570\\u914d\\u7f6e\",\"url\":\"\",\"icon\":\"\",\"type\":\"nav\",\"sort_order\":\"2\"}', '修改了权限规则', 1622621187);
INSERT INTO `hq_admin_log` VALUES (1723, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '59.61.174.14', 'http://admin.ennn.cn/admin/auth/addrule.html?pid=92', 'POST', 'json', '{\"pid\":\"92\",\"name\":\"\\u5b58\\u50a8\\u914d\\u7f6e\",\"url\":\"admin\\/config\\/param?model=qiniu\",\"icon\":\"\",\"type\":\"nav\",\"sort_order\":\"0\"}', '添加了权限规则', 1622621214);
INSERT INTO `hq_admin_log` VALUES (1724, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '59.61.174.14', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622623527);
INSERT INTO `hq_admin_log` VALUES (1725, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '59.61.174.14', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622623839);
INSERT INTO `hq_admin_log` VALUES (1726, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '59.61.174.14', 'http://admin.ennn.cn/admin/config/upload.html', 'POST', 'json', '{\"is_thumb\":\"1\",\"max_width\":\"200\",\"max_height\":\"200\"}', '修改了上传设置', 1622623955);
INSERT INTO `hq_admin_log` VALUES (1727, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '59.61.174.14', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622623962);
INSERT INTO `hq_admin_log` VALUES (1728, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '59.61.174.14', 'http://admin.ennn.cn/admin/uploads/del.html?id=251', 'POST', 'json', '{\"id\":\"251\"}', '删除了图片', 1622624192);
INSERT INTO `hq_admin_log` VALUES (1729, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '59.61.174.14', 'http://admin.ennn.cn/admin/uploads/del.html?id=250', 'POST', 'json', '{\"id\":\"250\"}', '删除了图片', 1622624228);
INSERT INTO `hq_admin_log` VALUES (1730, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '59.61.174.14', 'http://admin.ennn.cn/admin/uploads/del.html?id=249', 'POST', 'json', '{\"id\":\"249\"}', '删除了图片', 1622624244);
INSERT INTO `hq_admin_log` VALUES (1731, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '59.61.174.14', 'http://admin.ennn.cn/admin/uploads/del.html?id=248', 'POST', 'json', '{\"id\":\"248\"}', '删除了图片', 1622624248);
INSERT INTO `hq_admin_log` VALUES (1732, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '59.61.174.14', 'http://admin.ennn.cn/admin/uploads/del.html?id=230', 'POST', 'json', '{\"id\":\"230\"}', '删除了图片', 1622624472);
INSERT INTO `hq_admin_log` VALUES (1733, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '59.61.174.14', 'http://admin.ennn.cn/admin/uploads/del.html?id=245', 'POST', 'json', '{\"id\":\"245\"}', '删除了图片', 1622624477);
INSERT INTO `hq_admin_log` VALUES (1734, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '59.61.174.14', 'http://admin.ennn.cn/admin/auth/addrule.html', 'POST', 'json', '{\"pid\":\"0\",\"name\":\"\\u7cfb\\u7edf\\u9996\\u9875\",\"url\":\"\",\"icon\":\"layui-icon-console\",\"type\":\"nav\",\"sort_order\":\"0\"}', '添加了权限规则', 1622624596);
INSERT INTO `hq_admin_log` VALUES (1735, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '59.61.174.14', 'http://admin.ennn.cn/admin/auth/editRule.html?id=228', 'POST', 'json', '{\"id\":\"228\",\"pid\":\"0\",\"name\":\"\\u7cfb\\u7edf\\u9996\\u9875\",\"url\":\"admin\\/index\\/home\",\"icon\":\"layui-icon-console\",\"type\":\"nav\",\"sort_order\":\"0\"}', '修改了权限规则', 1622624613);
INSERT INTO `hq_admin_log` VALUES (1736, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '59.61.174.14', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622625706);
INSERT INTO `hq_admin_log` VALUES (1737, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '59.61.174.14', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622625720);
INSERT INTO `hq_admin_log` VALUES (1738, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '59.61.174.14', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622625754);
INSERT INTO `hq_admin_log` VALUES (1739, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '59.61.174.14', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622625771);
INSERT INTO `hq_admin_log` VALUES (1740, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '59.61.174.14', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622625865);
INSERT INTO `hq_admin_log` VALUES (1741, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '59.61.174.14', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622625888);
INSERT INTO `hq_admin_log` VALUES (1742, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '59.61.174.14', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622626034);
INSERT INTO `hq_admin_log` VALUES (1743, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '59.61.174.14', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622626267);
INSERT INTO `hq_admin_log` VALUES (1744, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '59.61.174.14', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622626296);
INSERT INTO `hq_admin_log` VALUES (1745, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '59.61.174.14', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622626308);
INSERT INTO `hq_admin_log` VALUES (1746, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '59.61.174.14', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622626506);
INSERT INTO `hq_admin_log` VALUES (1747, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '59.61.174.14', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622626520);
INSERT INTO `hq_admin_log` VALUES (1748, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '59.61.174.14', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622626651);
INSERT INTO `hq_admin_log` VALUES (1749, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '59.61.174.14', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622626716);
INSERT INTO `hq_admin_log` VALUES (1750, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '59.61.174.14', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622626755);
INSERT INTO `hq_admin_log` VALUES (1751, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '59.61.174.14', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622626768);
INSERT INTO `hq_admin_log` VALUES (1752, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '59.61.174.14', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622626771);
INSERT INTO `hq_admin_log` VALUES (1753, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '220.200.62.15', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622638625);
INSERT INTO `hq_admin_log` VALUES (1754, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '220.200.62.15', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622638647);
INSERT INTO `hq_admin_log` VALUES (1755, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '220.200.62.15', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622638718);
INSERT INTO `hq_admin_log` VALUES (1756, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '220.200.62.15', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622638766);
INSERT INTO `hq_admin_log` VALUES (1757, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '220.200.62.15', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622638792);
INSERT INTO `hq_admin_log` VALUES (1758, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '220.200.62.15', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622638811);
INSERT INTO `hq_admin_log` VALUES (1759, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '220.200.62.15', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622638848);
INSERT INTO `hq_admin_log` VALUES (1760, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '220.200.62.15', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622638966);
INSERT INTO `hq_admin_log` VALUES (1761, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '220.200.62.15', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622638983);
INSERT INTO `hq_admin_log` VALUES (1762, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '220.200.62.15', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622639018);
INSERT INTO `hq_admin_log` VALUES (1763, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '220.200.62.15', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622639022);
INSERT INTO `hq_admin_log` VALUES (1764, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '220.200.62.15', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622639050);
INSERT INTO `hq_admin_log` VALUES (1765, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '220.200.62.15', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622639055);
INSERT INTO `hq_admin_log` VALUES (1766, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '220.200.62.15', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622639086);
INSERT INTO `hq_admin_log` VALUES (1767, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '220.200.62.15', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622639119);
INSERT INTO `hq_admin_log` VALUES (1768, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '220.200.62.15', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622639123);
INSERT INTO `hq_admin_log` VALUES (1769, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '220.200.62.15', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622639656);
INSERT INTO `hq_admin_log` VALUES (1770, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '220.200.62.15', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622640253);
INSERT INTO `hq_admin_log` VALUES (1771, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '220.200.62.15', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622640280);
INSERT INTO `hq_admin_log` VALUES (1772, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '220.200.62.15', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622640343);
INSERT INTO `hq_admin_log` VALUES (1773, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '220.200.62.15', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622640396);
INSERT INTO `hq_admin_log` VALUES (1774, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '220.200.62.15', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622640403);
INSERT INTO `hq_admin_log` VALUES (1775, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '220.200.62.15', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622641029);
INSERT INTO `hq_admin_log` VALUES (1776, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '220.200.62.15', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622641084);
INSERT INTO `hq_admin_log` VALUES (1777, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '220.200.62.15', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622641447);
INSERT INTO `hq_admin_log` VALUES (1778, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '220.200.62.15', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622641606);
INSERT INTO `hq_admin_log` VALUES (1779, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '220.200.62.15', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622641644);
INSERT INTO `hq_admin_log` VALUES (1780, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '220.200.62.15', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622641668);
INSERT INTO `hq_admin_log` VALUES (1781, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '220.200.62.15', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622641680);
INSERT INTO `hq_admin_log` VALUES (1782, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '220.200.62.15', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622641747);
INSERT INTO `hq_admin_log` VALUES (1783, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '220.200.62.15', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622641835);
INSERT INTO `hq_admin_log` VALUES (1784, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '220.200.62.15', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622642124);
INSERT INTO `hq_admin_log` VALUES (1785, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '220.200.62.15', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622642359);
INSERT INTO `hq_admin_log` VALUES (1786, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '220.200.62.15', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622642425);
INSERT INTO `hq_admin_log` VALUES (1787, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '220.200.62.15', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622642456);
INSERT INTO `hq_admin_log` VALUES (1788, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '220.200.62.15', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622642476);
INSERT INTO `hq_admin_log` VALUES (1789, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '220.200.62.15', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622642551);
INSERT INTO `hq_admin_log` VALUES (1790, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '220.200.62.15', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622642584);
INSERT INTO `hq_admin_log` VALUES (1791, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '220.200.62.15', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622642634);
INSERT INTO `hq_admin_log` VALUES (1792, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '220.200.62.15', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622642656);
INSERT INTO `hq_admin_log` VALUES (1793, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '220.200.62.15', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622642696);
INSERT INTO `hq_admin_log` VALUES (1794, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '220.200.62.15', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622642807);
INSERT INTO `hq_admin_log` VALUES (1795, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '220.200.62.15', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622642835);
INSERT INTO `hq_admin_log` VALUES (1796, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '220.200.62.15', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622642859);
INSERT INTO `hq_admin_log` VALUES (1797, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '220.200.62.15', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622642952);
INSERT INTO `hq_admin_log` VALUES (1798, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '220.200.62.15', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622642957);
INSERT INTO `hq_admin_log` VALUES (1799, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '220.200.62.15', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622642967);
INSERT INTO `hq_admin_log` VALUES (1800, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '220.200.62.15', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622642983);
INSERT INTO `hq_admin_log` VALUES (1801, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '220.200.62.15', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622643018);
INSERT INTO `hq_admin_log` VALUES (1802, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '220.200.62.15', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622643029);
INSERT INTO `hq_admin_log` VALUES (1803, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '220.200.62.15', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622643064);
INSERT INTO `hq_admin_log` VALUES (1804, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '220.200.62.15', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622643076);
INSERT INTO `hq_admin_log` VALUES (1805, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '220.200.62.15', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622643267);
INSERT INTO `hq_admin_log` VALUES (1806, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '220.200.62.15', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622643307);
INSERT INTO `hq_admin_log` VALUES (1807, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '220.200.62.15', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622643369);
INSERT INTO `hq_admin_log` VALUES (1808, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '220.200.62.15', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622643533);
INSERT INTO `hq_admin_log` VALUES (1809, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '220.200.62.15', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622643550);
INSERT INTO `hq_admin_log` VALUES (1810, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '220.200.62.15', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622643563);
INSERT INTO `hq_admin_log` VALUES (1811, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '220.200.62.15', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622643965);
INSERT INTO `hq_admin_log` VALUES (1812, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '220.200.62.15', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622643978);
INSERT INTO `hq_admin_log` VALUES (1813, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '220.200.62.15', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622644020);
INSERT INTO `hq_admin_log` VALUES (1814, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36', '220.200.62.15', 'http://admin.ennn.cn/admin/index/login.html', 'POST', 'json', '{\"username\":\"admin\",\"password\":\"123456\",\"remember_user\":\"on\"}', '登录了后台系统', 1622644085);
INSERT INTO `hq_admin_log` VALUES (1815, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36', '220.200.62.15', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622644099);
INSERT INTO `hq_admin_log` VALUES (1816, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36', '220.200.62.15', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622644414);
INSERT INTO `hq_admin_log` VALUES (1817, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '220.200.62.15', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622644734);
INSERT INTO `hq_admin_log` VALUES (1818, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '220.200.62.15', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622644746);
INSERT INTO `hq_admin_log` VALUES (1819, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '220.200.62.15', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622644763);
INSERT INTO `hq_admin_log` VALUES (1820, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '220.200.62.15', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622644771);
INSERT INTO `hq_admin_log` VALUES (1821, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '220.200.62.15', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622644782);
INSERT INTO `hq_admin_log` VALUES (1822, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '220.200.62.15', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622644820);
INSERT INTO `hq_admin_log` VALUES (1823, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '220.200.62.15', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622644904);
INSERT INTO `hq_admin_log` VALUES (1824, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '220.200.62.15', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622644930);
INSERT INTO `hq_admin_log` VALUES (1825, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '220.200.62.15', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622645000);
INSERT INTO `hq_admin_log` VALUES (1826, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '220.200.62.15', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622645016);
INSERT INTO `hq_admin_log` VALUES (1827, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '220.200.62.15', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622645044);
INSERT INTO `hq_admin_log` VALUES (1828, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '220.200.62.15', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622645076);
INSERT INTO `hq_admin_log` VALUES (1829, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '220.200.62.15', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622645107);
INSERT INTO `hq_admin_log` VALUES (1830, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '220.200.62.15', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622645310);
INSERT INTO `hq_admin_log` VALUES (1831, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '220.200.62.15', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622645455);
INSERT INTO `hq_admin_log` VALUES (1832, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '220.200.62.15', 'http://admin.ennn.cn/admin/config/upload.html', 'POST', 'json', '{\"is_thumb\":\"0\",\"max_width\":\"200\",\"max_height\":\"200\"}', '修改了上传设置', 1622645469);
INSERT INTO `hq_admin_log` VALUES (1833, 1, 'admin', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400', '220.200.62.15', 'http://admin.ennn.cn/admin/uploads/uploadImage', 'POST', 'json', '[]', '上传了图片', 1622645478);

-- ----------------------------
-- Table structure for hq_article
-- ----------------------------
DROP TABLE IF EXISTS `hq_article`;
CREATE TABLE `hq_article`  (
  `id` int(8) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_id` int(5) NOT NULL DEFAULT 0 COMMENT 'ID',
  `cid` int(5) NOT NULL COMMENT '分类ID',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题',
  `keyword` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '关键字',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `author` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '作者',
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '封面',
  `source` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '来源',
  `view` int(10) NOT NULL DEFAULT 0 COMMENT '阅读量',
  `content` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '详细内容',
  `sort_order` tinyint(5) NOT NULL DEFAULT 100 COMMENT '排序',
  `status` tinyint(5) NOT NULL DEFAULT 0 COMMENT '状态 0待审核 1审核通过 2审核拒绝',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'URL',
  `is_link` tinyint(3) NOT NULL DEFAULT 0 COMMENT '是否链接 0不链接 1链接',
  `is_hot` tinyint(3) NOT NULL DEFAULT 0 COMMENT '是否热门 0不热门 1热门',
  `is_tuijian` tinyint(3) NOT NULL DEFAULT 0 COMMENT '是否推荐 0不推荐 1推荐',
  `create_time` int(11) NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(11) NOT NULL DEFAULT 0 COMMENT '更新时间',
  `delete_time` int(11) NOT NULL DEFAULT 0 COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 136 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '文章表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of hq_article
-- ----------------------------
INSERT INTO `hq_article` VALUES (128, 0, 66, '定时宝 定时提醒助手', '是打发', '定时提醒助手', '', 'https://mdf.muxue.com.cn/uploads/20200812/c11f84f725bee7e1cc53e14a693c0b56.jpg', '', 53, '<p>定时宝</p>\r\n<p>定时提醒小程序</p>\r\n<p>主要为商家提供定时提醒服务,客户管理服务等</p>', 100, 1, '/show/128.html', 0, 0, 0, 1597126342, 1597230393, 0);
INSERT INTO `hq_article` VALUES (129, 0, 66, '出纳宝 小微企业记账软件', '啥是打发啥都,地方,啊', '小微企业记账软件', '', 'https://mdf.muxue.com.cn/uploads/20200812/db9d2eaae7994e9a85cf69aa72488347.jpg', '', 111, '<p>出纳宝</p>\r\n<p>针对老板，无需任何会计知识，轻松管账，管钱，管库存，从此告别糊涂账。针对财务人员，凭证，报表自动生成，简单省事。</p>\r\n<div class=\"section-heading\">\r\n<h2 class=\"title\">无需任何会计知识,轻松记账</h2>\r\n<p class=\"text\">一键添加收入支出，智能管理应收应付，轻松解决复杂的往来账务。</p>\r\n</div>\r\n<ul class=\"feature-list\">\r\n<li>\r\n<p>多端同步记账，多人同步协作。</p>\r\n</li>\r\n<li>\r\n<p>一键记录收入支出，统计往来账目</p>\r\n</li>\r\n<li>\r\n<p>待收待付，定时提醒，杜绝坏账。</p>\r\n</li>\r\n<li>\r\n<p>一键导入，实时导出，数据安全有保</p>\r\n</li>\r\n</ul>', 100, 1, '/show/129.html', 0, 0, 0, 1597126376, 1617677487, 0);
INSERT INTO `hq_article` VALUES (130, 0, 66, '课时邦 小培训机构销课系统', '是打发', '小培训机构销课系统', '', 'https://mdf.muxue.com.cn/uploads/20200814/c6a4a19fafbe3c22351af7e4432c3c5e.png', '', 80, '<p>定时宝</p>\r\n<p>定时提醒小程序</p>\r\n<p>主要为商家提供定时提醒服务,客户管理服务等</p>', 100, 1, '/show/130.html', 0, 0, 0, 1597126342, 1613655546, 0);
INSERT INTO `hq_article` VALUES (131, 0, 66, '云代理商 小程序分销系统', '啥是打发啥都,地方,啊', '小程序分销系统', '', 'https://mdf.muxue.com.cn/uploads/20200814/fb6e491120001bbb7d6611be3046a75d.png', '', 174, '<table style=\"width: 100%;\" border=\"1\">\r\n<tbody>\r\n<tr>\r\n<td style=\"width: 50%;\">&nbsp;</td>\r\n<td style=\"width: 50%;\">&nbsp;</td>\r\n</tr>\r\n<tr>\r\n<td style=\"width: 50%;\">&nbsp;</td>\r\n<td style=\"width: 50%;\">&nbsp;</td>\r\n</tr>\r\n</tbody>\r\n</table>', 100, 1, '/show/131.html', 0, 0, 0, 1597126376, 1621086236, 0);
INSERT INTO `hq_article` VALUES (132, 0, 66, 'test', '', '', '', '', '', 0, '<p><img src=\"http://www.quwenqushi.com/uploadfile/2020/0922/20200922083115263.jpg\" /></p>', 100, 1, '/show/132.html', 0, 0, 0, 1600828172, 1600828353, 1600828353);
INSERT INTO `hq_article` VALUES (133, 0, 66, 'we ', '', '', '', '', '', 0, '', 100, 1, '/show/133.html', 0, 0, 0, 1611551347, 1611551357, 1611551357);
INSERT INTO `hq_article` VALUES (134, 0, 66, '12313', '', '', '', '', '', 0, '<p>123123123121231231</p>', 100, 1, '/show/134.html', 0, 0, 0, 1616135645, 1622618593, 1622618593);
INSERT INTO `hq_article` VALUES (135, 0, 67, '是否啊是打发阿道夫', '', '', '', '', '', 0, '<p>是打发是打发发所发生的沙发dsf</p>\r\n<p>沙发</p>\r\n<p>a是fa发</p>\r\n<p>阿斯顿发的</p>\r\n<p>啊</p>\r\n<p>fas&nbsp; 沙发是打发</p>\r\n<p>啊</p>\r\n<p>沙发大啊是打发啊ad发啊发送</p>', 100, 1, '/show/135.html', 0, 0, 0, 1622616136, 1622616136, 0);

-- ----------------------------
-- Table structure for hq_auth_group
-- ----------------------------
DROP TABLE IF EXISTS `hq_auth_group`;
CREATE TABLE `hq_auth_group`  (
  `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `rules` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '权限组' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of hq_auth_group
-- ----------------------------
INSERT INTO `hq_auth_group` VALUES (1, '超级管理员', '', 1, '6,43,44,168,178,179,199,200,201,202,203,204,205,206,207,219,180,142,143,145,146,147,186,188,182,183,184,185,155,156,158,159,160,157,161,162,163,174,175,176,177,195,196,197,198,164,165,166,167,169,181,194,172,170,171,173,187,189,191,192,193,190,4,123,125,126,127,128,92,45,68,69,70,103,5,16,37,38,39,18,53,17,40,41,42,15,22,23,24');
INSERT INTO `hq_auth_group` VALUES (4, '二级学院', '', 1, '6,43,44,142,143,145,146,147');
INSERT INTO `hq_auth_group` VALUES (5, '联系人', '', 1, '142,143,145,146,147');
INSERT INTO `hq_auth_group` VALUES (6, '学生', '', 1, '5,16,37,38,39,18,53,17,40,41,42,15,22,23,24');

-- ----------------------------
-- Table structure for hq_auth_group_access
-- ----------------------------
DROP TABLE IF EXISTS `hq_auth_group_access`;
CREATE TABLE `hq_auth_group_access`  (
  `uid` smallint(5) UNSIGNED NOT NULL DEFAULT 0,
  `group_id` smallint(5) UNSIGNED NOT NULL DEFAULT 0
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '权限授权' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of hq_auth_group_access
-- ----------------------------
INSERT INTO `hq_auth_group_access` VALUES (1, 1);
INSERT INTO `hq_auth_group_access` VALUES (2, 1);
INSERT INTO `hq_auth_group_access` VALUES (7, 4);

-- ----------------------------
-- Table structure for hq_auth_rule
-- ----------------------------
DROP TABLE IF EXISTS `hq_auth_rule`;
CREATE TABLE `hq_auth_rule`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `pid` int(11) UNSIGNED NOT NULL DEFAULT 0,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `icon` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `sort_order` int(11) NOT NULL DEFAULT 0 COMMENT '排序',
  `type` char(4) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'nav,auth',
  `index` tinyint(1) NOT NULL DEFAULT 0 COMMENT '快捷导航',
  `status` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 229 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '权限规则' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of hq_auth_rule
-- ----------------------------
INSERT INTO `hq_auth_rule` VALUES (4, 0, '系统设置', '', 'layui-icon-set', 6, 'nav', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (5, 0, '管理员管理', '', 'layui-icon-auz', 9, 'nav', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (6, 0, '控制台', 'admin/index/home', 'layui-icon-home', 0, 'nav', 0, 0);
INSERT INTO `hq_auth_rule` VALUES (15, 5, '权限规则', 'admin/auth/rule', 'fa fa-th-list', 3, 'nav', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (16, 5, '管理员列表', 'admin/admin/index', 'fa fa-user', 0, 'nav', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (17, 5, '角色管理', 'admin/auth/group', 'fa fa-users', 2, 'nav', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (18, 5, '操作日志', 'admin/admin/log', 'fa fa-clock-o', 1, 'nav', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (22, 15, '添加', 'admin/auth/addRule', '', 0, 'auth', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (23, 15, '编辑', 'admin/auth/editRule', '', 0, 'auth', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (24, 15, '删除', 'admin/auth/delRule', '', 0, 'auth', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (37, 16, '添加', 'admin/admin/add', '', 0, 'auth', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (38, 16, '编辑', 'admin/admin/edit', '', 0, 'auth', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (39, 16, '删除', 'admin/admin/del', '', 0, 'auth', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (40, 17, '添加', 'admin/auth/addGroup', '', 0, 'auth', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (41, 17, '编辑', 'admin/auth/editGroup', '', 0, 'auth', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (42, 17, '删除', 'admin/auth/delGroup', '', 0, 'auth', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (43, 6, '修改密码', 'admin/index/editPassword', '', 2, 'auth', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (44, 6, '清除缓存', 'admin/index/clear', '', 3, 'auth', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (45, 4, '上传设置', 'admin/config/upload', 'fa fa-upload', 4, 'nav', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (53, 18, '一键清空', 'admin/admin/truncate', '', 0, 'auth', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (68, 45, '上传图片', 'admin/index/uploadimage', '', 0, 'auth', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (69, 45, '上传文件', 'admin/index/uploadfile', '', 0, 'auth', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (70, 45, '上传视频', 'admin/index/uploadvideo', '', 0, 'auth', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (92, 4, '参数配置', '', '', 2, 'nav', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (103, 4, '上传管理', 'admin/uploads/index', '', 10, 'nav', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (123, 4, '网站配置', 'admin/config/setting', '', 0, 'nav', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (125, 4, '字典管理', 'admin/dict/index', '', 0, 'nav', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (126, 125, '新增', 'admin/dict/add', '', 0, 'auth', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (127, 125, '编辑', 'admin/dict/edit', '', 0, 'auth', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (128, 125, '删除', 'admin/dict/del', '', 0, 'auth', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (142, 0, '会员管理', '', 'layui-icon-user', 3, 'nav', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (143, 142, '会员管理', 'admin/user/index', '', 0, 'nav', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (145, 143, '添加', 'admin/user/add', '', 0, 'auth', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (146, 143, '编辑', 'admin/user/edit', '', 0, 'auth', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (147, 143, '删除', 'admin/user/del', '', 0, 'auth', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (155, 0, '内容管理', '', 'layui-icon-read', 4, 'nav', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (156, 155, '栏目管理', 'admin/category/index', '', 0, 'nav', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (157, 155, '内容管理', 'admin/article/index', '', 0, 'nav', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (158, 156, '添加', 'admin/category/add', '', 0, 'auth', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (159, 156, '编辑', 'admin/category/edit', '', 0, 'auth', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (160, 156, '删除', 'admin/category/del', '', 0, 'auth', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (161, 157, '添加', 'admin/article/add', '', 0, 'auth', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (162, 157, '编辑', 'admin/article/edit', '', 0, 'auth', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (163, 157, '删除', 'admin/article/del', '', 0, 'auth', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (164, 155, '轮播管理', 'admin/ad/index', '', 100, 'nav', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (223, 92, '存储配置', 'admin/config/param?model=storage', '', 0, 'nav', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (224, 92, '公众号配置', 'admin/config/param?model=weixin', '', 0, 'nav', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (225, 92, '小程序配置', 'admin/config/param?model=wxapp', '', 0, 'nav', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (226, 92, '微信支付配置', 'admin/config/param?model=wxpay', '', 0, 'nav', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (227, 92, '短信配置', 'admin/config/param?model=sms', '', 0, 'nav', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (228, 0, '系统首页', 'admin/index/home', 'layui-icon-console', 0, 'nav', 0, 1);

-- ----------------------------
-- Table structure for hq_category
-- ----------------------------
DROP TABLE IF EXISTS `hq_category`;
CREATE TABLE `hq_category`  (
  `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `pid` int(5) NOT NULL DEFAULT 0 COMMENT '上级栏目',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '分类名称',
  `keywords` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '关键字',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `name_en` char(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '分类英文',
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '分类图片',
  `sort_order` tinyint(10) NOT NULL DEFAULT 100 COMMENT '排序',
  `status` tinyint(5) NOT NULL DEFAULT 1 COMMENT '状态',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '页面URL',
  `model` tinyint(5) NULL DEFAULT NULL COMMENT '模型类型',
  `template` char(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '1' COMMENT '分类模板',
  `is_tuijian` tinyint(3) NOT NULL DEFAULT 0 COMMENT '是否推荐 0不推荐 1推荐',
  `create_time` int(10) NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(10) NOT NULL DEFAULT 0 COMMENT '更新时间',
  `delete_time` int(10) NOT NULL DEFAULT 0 COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 71 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of hq_category
-- ----------------------------
INSERT INTO `hq_category` VALUES (66, 0, '文章', '', '', '', '', 100, 1, '/lists/66.html', 1, 'lists', 0, 1622610426, 1622610426, 0);
INSERT INTO `hq_category` VALUES (67, 0, '跳转', '', '', '', '', 100, 1, '/lists/67.html', 1, 'lists', 0, 1622610558, 1622610672, 0);
INSERT INTO `hq_category` VALUES (68, 0, '关于我们', '', '', '', '', 100, 1, '/lists/68.html', 2, 'lists', 0, 1622617088, 1622617088, 0);
INSERT INTO `hq_category` VALUES (69, 0, '联系我们', '', '', '', '', 100, 1, '/lists/69.html', 2, 'lists', 0, 1622617720, 1622617720, 0);
INSERT INTO `hq_category` VALUES (70, 0, '招聘信息', '', '', '', '', 100, 1, '/lists/70.html', 2, 'lists', 0, 1622617800, 1622617800, 0);

-- ----------------------------
-- Table structure for hq_dict
-- ----------------------------
DROP TABLE IF EXISTS `hq_dict`;
CREATE TABLE `hq_dict`  (
  `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字典类目名称',
  `sort_order` tinyint(5) NULL DEFAULT 10 COMMENT '排序',
  `is_system` tinyint(5) NOT NULL DEFAULT 0 COMMENT '系统 0否 1是',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '字典' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of hq_dict
-- ----------------------------
INSERT INTO `hq_dict` VALUES (1, '栏目模型', 10, 1);
INSERT INTO `hq_dict` VALUES (14, '轮播分类', 10, 0);

-- ----------------------------
-- Table structure for hq_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `hq_dict_data`;
CREATE TABLE `hq_dict_data`  (
  `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `dict_id` int(3) NOT NULL DEFAULT 0 COMMENT '字典ID',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '字典名称',
  `value` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字段值',
  `sort_order` tinyint(10) NOT NULL DEFAULT 0 COMMENT '排序',
  `status` tinyint(10) NOT NULL DEFAULT 1 COMMENT '状态 0禁用 1启用',
  `is_system` tinyint(10) NOT NULL DEFAULT 0 COMMENT '系统字段 0否 1是',
  `create_time` int(10) NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(10) NOT NULL DEFAULT 0 COMMENT '更新时间',
  `delete_time` int(10) NOT NULL DEFAULT 0 COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 51 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '字典数据' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of hq_dict_data
-- ----------------------------
INSERT INTO `hq_dict_data` VALUES (1, 1, '文章模型', 'article', 10, 1, 1, 1622609323, 1622609946, 0);
INSERT INTO `hq_dict_data` VALUES (2, 1, '单页模型', 'page', 9, 1, 1, 1622609361, 1622609950, 0);
INSERT INTO `hq_dict_data` VALUES (3, 1, '图片模型', 'picture', 8, 0, 1, 1622609336, 1622617313, 0);
INSERT INTO `hq_dict_data` VALUES (4, 1, '产品模型', 'product', 7, 0, 1, 1622609345, 1622617313, 0);
INSERT INTO `hq_dict_data` VALUES (5, 1, '跳转链接', NULL, 6, 1, 1, 1622609356, 1622609958, 0);
INSERT INTO `hq_dict_data` VALUES (50, 14, '首页轮播', '', 0, 1, 0, 1622618980, 1622618980, 0);

-- ----------------------------
-- Table structure for hq_page
-- ----------------------------
DROP TABLE IF EXISTS `hq_page`;
CREATE TABLE `hq_page`  (
  `id` int(8) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_id` int(5) NOT NULL DEFAULT 0 COMMENT 'ID',
  `cid` int(5) NOT NULL COMMENT '分类ID',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题',
  `keyword` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '关键字',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `author` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '作者',
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '封面',
  `source` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '来源',
  `view` int(10) NOT NULL DEFAULT 0 COMMENT '阅读量',
  `content` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '详细内容',
  `sort_order` tinyint(5) NOT NULL DEFAULT 100 COMMENT '排序',
  `status` tinyint(5) NOT NULL DEFAULT 0 COMMENT '状态 0待审核 1审核通过 2审核拒绝',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'URL',
  `is_link` tinyint(3) NOT NULL DEFAULT 0 COMMENT '是否链接 0不链接 1链接',
  `is_hot` tinyint(3) NOT NULL DEFAULT 0 COMMENT '是否热门 0不热门 1热门',
  `is_tuijian` tinyint(3) NOT NULL DEFAULT 0 COMMENT '是否推荐 0不推荐 1推荐',
  `create_time` int(11) NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(11) NOT NULL DEFAULT 0 COMMENT '更新时间',
  `delete_time` int(11) NOT NULL DEFAULT 0 COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 139 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '文章表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of hq_page
-- ----------------------------
INSERT INTO `hq_page` VALUES (136, 1, 68, '阿萨德法师打发', '', '', '', '', '', 0, '<p>阿萨德法师打发啊发发顺丰啊是打发啊是打发沙发阿诗丹顿发是打发大</p>', 100, 1, '/page/136.html', 0, 0, 0, 1622617169, 1622617169, 0);
INSERT INTO `hq_page` VALUES (137, 0, 69, '联系我们', NULL, NULL, NULL, NULL, NULL, 0, NULL, 100, 1, '/page/137.html', 0, 0, 0, 1622617723, 1622618487, 0);
INSERT INTO `hq_page` VALUES (138, 0, 70, '招聘信息', '', '', NULL, '', NULL, 0, '<p>阿斯顿发送到沙发大啊是短发水电费啊阿诗丹顿发打发阿斯顿发达是打发大&nbsp;是打发啊</p>', 100, 1, '/page/138.html', 0, 0, 0, 1622617803, 1622618496, 0);

-- ----------------------------
-- Table structure for hq_system
-- ----------------------------
DROP TABLE IF EXISTS `hq_system`;
CREATE TABLE `hq_system`  (
  `id` int(3) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `key` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '配置字段',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '配置名称',
  `jdata` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '配置参数',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统配置表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of hq_system
-- ----------------------------
INSERT INTO `hq_system` VALUES (1, 'website', '网站配置', '{\"logo\":\"\",\"sitename\":\"1\",\"title\":\"323\",\"keywords\":\"13\",\"description\":\"422\",\"copyright\":\"5sddfas\",\"beian\":\"6sad\"}');
INSERT INTO `hq_system` VALUES (2, 'storage', '存储配置', '{\"model\":\"storage\",\"type\":\"local\",\"ak\":\"1\",\"sk\":\"2\",\"bucket\":\"3\",\"domain\":\"http:\\/\\/img.ennn.cn\\/4\"}');
INSERT INTO `hq_system` VALUES (3, 'weixin', '公众号配置', '{\"model\":\"weixin\",\"appid\":\"11\",\"appsecret\":\"22\",\"token\":\"liunuoyun332\",\"aes_key\":\"44\",\"type\":\"0\"}');
INSERT INTO `hq_system` VALUES (4, 'wxapp', '小程序配置', '{\"model\":\"wxapp\",\"appid\":\"44\",\"appsecret\":\"55\",\"type\":\"0\"}');
INSERT INTO `hq_system` VALUES (5, 'wxpay', '微信支付配置', '{\"model\":\"wxpay\",\"appid\":\"66\",\"mch_id\":\"77\",\"key\":\"88\",\"notify_url\":\"4\",\"certpem\":\"111\",\"keypem\":\"333\",\"type\":\"0\"}');
INSERT INTO `hq_system` VALUES (6, 'sms', '腾讯云短信', NULL);
INSERT INTO `hq_system` VALUES (10, 'upload_setting', '上传选项', '{\"is_thumb\":\"0\",\"max_width\":\"200\",\"max_height\":\"200\"}');
INSERT INTO `hq_system` VALUES (11, 'system', '系统配置', '{\"title\":\"\\u4e91\\u4ee3\\u7406\\u5546\\u7cfb\\u7edf\",\"custom_image\":\"https:\\/\\/mdf.muxue.com.cn\\/uploads\\/20200913\\/e4e3a9cae949578bd9a09303b9efa843.png\",\"custom_color\":\"#8dc63f\",\"custom_textcolor\":\"#333333\",\"color\":\"#f37b1d\",\"is_back\":\"on\"}');

-- ----------------------------
-- Table structure for hq_table
-- ----------------------------
DROP TABLE IF EXISTS `hq_table`;
CREATE TABLE `hq_table`  (
  `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `table` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表名',
  `create_time` int(11) NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(11) NULL DEFAULT 0 COMMENT '更新时间',
  `delete_time` int(11) NULL DEFAULT 0 COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '自定义表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of hq_table
-- ----------------------------
INSERT INTO `hq_table` VALUES (1, '科研成果-纵向项目', 'achievements_zongxiang', 0, 0, 0);
INSERT INTO `hq_table` VALUES (2, '科研成果-横向项目', 'achievements_hengxiang', 0, 0, 0);

-- ----------------------------
-- Table structure for hq_table_field
-- ----------------------------
DROP TABLE IF EXISTS `hq_table_field`;
CREATE TABLE `hq_table_field`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `table_id` int(10) NULL DEFAULT NULL COMMENT '自定义表ID',
  `field` char(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字段名称',
  `type` char(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字段类型',
  `length` int(5) NULL DEFAULT NULL COMMENT '长度',
  `point` int(5) NULL DEFAULT NULL COMMENT '小数点',
  `collation` char(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字符序',
  `null` char(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否为空',
  `key` char(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '主键',
  `default` char(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '默认值',
  `extra` char(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '自增长',
  `privileges` char(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限',
  `comment` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '注释',
  `input` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'input' COMMENT '输入框格式',
  `sort_order` int(10) NULL DEFAULT 100 COMMENT '排序',
  `status` int(5) NULL DEFAULT 1 COMMENT '显示 0不显示 1显示',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 490 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '自定义表字段' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of hq_table_field
-- ----------------------------
INSERT INTO `hq_table_field` VALUES (449, 1, 'id', 'int', 10, NULL, NULL, 'NO', 'PRI', NULL, 'auto_increment', 'select,insert,update,reference', 'ID', 'input', 100, 0);
INSERT INTO `hq_table_field` VALUES (450, 1, 'user_id', 'int', 10, NULL, NULL, 'YES', '', '0', '', 'select,insert,update,reference', '操作管理员', 'input', 100, 0);
INSERT INTO `hq_table_field` VALUES (451, 1, 'create_time', 'int', 11, NULL, NULL, 'YES', '', '0', '', 'select,insert,update,reference', '创建时间', 'input', 100, 0);
INSERT INTO `hq_table_field` VALUES (452, 1, 'update_time', 'int', 11, NULL, NULL, 'YES', '', '0', '', 'select,insert,update,reference', '更新时间', 'input', 100, 0);
INSERT INTO `hq_table_field` VALUES (453, 1, 'delete_time', 'int', 11, NULL, NULL, 'YES', '', '0', '', 'select,insert,update,reference', '删除时间', 'input', 100, 0);
INSERT INTO `hq_table_field` VALUES (454, 1, 'project_sn', 'char', 30, NULL, 'utf8_general_ci', 'YES', '', NULL, '', 'select,insert,update,reference', '项目(课题)编号', 'input', 100, 1);
INSERT INTO `hq_table_field` VALUES (455, 1, 'document_sn', 'char', 50, NULL, 'utf8_general_ci', 'YES', '', NULL, '', 'select,insert,update,reference', '下达文号', 'input', 100, 1);
INSERT INTO `hq_table_field` VALUES (456, 1, 'leading_name', 'char', 50, NULL, 'utf8_general_ci', 'YES', '', NULL, '', 'select,insert,update,reference', '负责人', 'input', 100, 1);
INSERT INTO `hq_table_field` VALUES (457, 1, 'name', 'varchar', 100, NULL, 'utf8_general_ci', 'YES', '', NULL, '', 'select,insert,update,reference', '项目名称', 'input', 100, 1);
INSERT INTO `hq_table_field` VALUES (458, 1, 'comefrom', 'varchar', 100, NULL, 'utf8_general_ci', 'YES', '', NULL, '', 'select,insert,update,reference', '项目来源', 'input', 100, 1);
INSERT INTO `hq_table_field` VALUES (459, 1, 'type', 'varchar', 100, NULL, 'utf8_general_ci', 'YES', '', NULL, '', 'select,insert,update,reference', '项目类型', 'input', 100, 1);
INSERT INTO `hq_table_field` VALUES (460, 1, 'outlay', 'decimal', 10, 4, NULL, 'YES', '', NULL, '', 'select,insert,update,reference', '计划经费(万元)', 'input', 100, 1);
INSERT INTO `hq_table_field` VALUES (461, 1, 'college', 'varchar', 100, 4, 'utf8_general_ci', 'YES', '', NULL, '', 'select,insert,update,reference', '学院部门', 'input', 100, 1);
INSERT INTO `hq_table_field` VALUES (462, 1, 'gender', 'char', 20, 4, 'utf8_general_ci', 'YES', '', NULL, '', 'select,insert,update,reference', '性别', 'input', 100, 1);
INSERT INTO `hq_table_field` VALUES (463, 1, 'person', 'varchar', 255, 4, 'utf8_general_ci', 'YES', '', NULL, '', 'select,insert,update,reference', '参加人员(按排名)', 'input', 100, 1);
INSERT INTO `hq_table_field` VALUES (464, 1, 'start_year', 'year', 4, 4, NULL, 'YES', '', NULL, '', 'select,insert,update,reference', '起止年限', 'input', 100, 1);
INSERT INTO `hq_table_field` VALUES (465, 1, 'end_year', 'year', 4, 4, NULL, 'YES', '', NULL, '', 'select,insert,update,reference', '起止年限', 'input', 100, 1);
INSERT INTO `hq_table_field` VALUES (466, 1, 'level', 'varchar', 50, 4, 'utf8_general_ci', 'YES', '', NULL, '', 'select,insert,update,reference', '级别', 'input', 100, 1);
INSERT INTO `hq_table_field` VALUES (467, 1, 'partner', 'varchar', 100, 4, 'utf8_general_ci', 'YES', '', NULL, '', 'select,insert,update,reference', '合作单位', 'input', 100, 1);
INSERT INTO `hq_table_field` VALUES (468, 1, 'dial_out', 'decimal', 10, 4, NULL, 'YES', '', NULL, '', 'select,insert,update,reference', '外拨(万元)', 'input', 100, 1);
INSERT INTO `hq_table_field` VALUES (469, 1, 'approval_time', 'int', 11, 4, NULL, 'YES', '', NULL, '', 'select,insert,update,reference', '立项时间', 'input', 100, 1);
INSERT INTO `hq_table_field` VALUES (470, 1, 'is_closed', 'char', 30, 4, 'utf8_general_ci', 'YES', '', NULL, '', 'select,insert,update,reference', '是否按期结题或延期', 'input', 100, 1);
INSERT INTO `hq_table_field` VALUES (472, 2, 'id', 'int', 10, NULL, NULL, 'NO', 'PRI', NULL, 'auto_increment', 'select,insert,update,reference', 'ID', 'input', 100, 0);
INSERT INTO `hq_table_field` VALUES (473, 2, 'user_id', 'int', 10, NULL, NULL, 'YES', '', '0', '', 'select,insert,update,reference', '操作管理员', 'input', 100, 0);
INSERT INTO `hq_table_field` VALUES (474, 2, 'create_time', 'int', 11, NULL, NULL, 'YES', '', '0', '', 'select,insert,update,reference', '创建时间', 'input', 100, 0);
INSERT INTO `hq_table_field` VALUES (475, 2, 'update_time', 'int', 11, NULL, NULL, 'YES', '', '0', '', 'select,insert,update,reference', '更新时间', 'input', 100, 0);
INSERT INTO `hq_table_field` VALUES (476, 2, 'delete_time', 'int', 11, NULL, NULL, 'YES', '', '0', '', 'select,insert,update,reference', '删除时间', 'input', 100, 0);
INSERT INTO `hq_table_field` VALUES (477, 2, 'reg_time', 'int', 11, NULL, NULL, 'YES', '', NULL, '', 'select,insert,update,reference', '登记日期', 'input', 100, 1);
INSERT INTO `hq_table_field` VALUES (478, 2, 'contract_no', 'varchar', 100, NULL, 'utf8_general_ci', 'YES', '', NULL, '', 'select,insert,update,reference', '合同编号', 'input', 100, 1);
INSERT INTO `hq_table_field` VALUES (479, 2, 'project_no', 'varchar', 100, NULL, 'utf8_general_ci', 'YES', '', NULL, '', 'select,insert,update,reference', '项目经费编号', 'input', 100, 1);
INSERT INTO `hq_table_field` VALUES (480, 2, 'contract_time', 'int', 11, NULL, NULL, 'YES', '', NULL, '', 'select,insert,update,reference', '合同签订日期', 'input', 100, 1);
INSERT INTO `hq_table_field` VALUES (481, 2, 'start_time', 'int', 11, NULL, NULL, 'YES', '', NULL, '', 'select,insert,update,reference', '合同期限', 'input', 100, 1);
INSERT INTO `hq_table_field` VALUES (482, 2, 'end_time', 'int', 11, NULL, NULL, 'YES', '', NULL, '', 'select,insert,update,reference', '合同结束日期', 'input', 100, 1);
INSERT INTO `hq_table_field` VALUES (483, 2, 'contract_attr', 'varchar', 100, NULL, 'utf8_general_ci', 'YES', '', NULL, '', 'select,insert,update,reference', '合同属性', 'input', 100, 1);
INSERT INTO `hq_table_field` VALUES (484, 2, 'client', 'varchar', 100, NULL, 'utf8_general_ci', 'YES', '', NULL, '', 'select,insert,update,reference', '委托单位', 'input', 100, 1);
INSERT INTO `hq_table_field` VALUES (485, 2, 'name', 'varchar', 100, NULL, 'utf8_general_ci', 'YES', '', NULL, '', 'select,insert,update,reference', '课题名称', 'input', 100, 1);
INSERT INTO `hq_table_field` VALUES (486, 2, 'leading_name', 'varchar', 100, NULL, 'utf8_general_ci', 'YES', '', NULL, '', 'select,insert,update,reference', '负责人', 'input', 100, 1);
INSERT INTO `hq_table_field` VALUES (487, 2, 'college', 'varchar', 100, NULL, 'utf8_general_ci', 'YES', '', NULL, '', 'select,insert,update,reference', '学院部门', 'input', 100, 1);
INSERT INTO `hq_table_field` VALUES (488, 2, 'outlay', 'decimal', 10, 4, NULL, 'YES', '', NULL, '', 'select,insert,update,reference', '总经费(万元)', 'input', 100, 1);
INSERT INTO `hq_table_field` VALUES (489, 1, 'remark', 'varchar', 255, 4, 'utf8_general_ci', 'YES', '', NULL, '', 'select,insert,update,reference', '备注', 'input', 100, 1);

-- ----------------------------
-- Table structure for hq_uploads
-- ----------------------------
DROP TABLE IF EXISTS `hq_uploads`;
CREATE TABLE `hq_uploads`  (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `shop_id` int(5) NULL DEFAULT 0 COMMENT '商户ID',
  `user_id` int(5) UNSIGNED NULL DEFAULT 0 COMMENT '用户ID',
  `storage` char(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '存储位置',
  `file_url` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '存储域名',
  `file_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文件名',
  `file_size` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文件大小',
  `mine` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` int(3) NULL DEFAULT 1 COMMENT '状态',
  `create_time` int(10) NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(10) NOT NULL DEFAULT 0 COMMENT '更新时间',
  `delete_time` int(10) NOT NULL DEFAULT 0 COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 348 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '上传数据表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of hq_uploads
-- ----------------------------
INSERT INTO `hq_uploads` VALUES (225, 0, 1, 'local', '', 'uploads/20200925/7b18b226c7fb6cd029bc6b5caeeb038d.png', '68182', 'image/png', 1, 1601042114, 1611674775, 1611674775);
INSERT INTO `hq_uploads` VALUES (226, 0, 1, 'local', '', 'uploads/20210220/045ed4704535141727c737f72fd7e987.png', '9068', 'image/png', 1, 1613792768, 1613801778, 1613801778);
INSERT INTO `hq_uploads` VALUES (227, 0, 1, 'local', '', 'uploads/20210220/b151e74935c2c4bb65eb70f2ea87541e.png', '44200', 'image/png', 1, 1613794043, 1613801788, 1613801788);
INSERT INTO `hq_uploads` VALUES (228, 0, 1, 'local', '', 'uploads/20210220/dd7cb692f56dbec5b337c50c025c8f3c.png', '10519', 'image/png', 1, 1613794046, 1613801785, 1613801785);
INSERT INTO `hq_uploads` VALUES (229, 0, 1, 'local', '', 'uploads/20210220/970c7b70fb6aa20ffc21f7645c9828ec.png', '9068', 'image/png', 1, 1613794078, 1613801781, 1613801781);
INSERT INTO `hq_uploads` VALUES (230, 0, 1, 'local', '', 'uploads/20210223/302b555219b4309dc6f610da2030d23e.JPG', '26797', 'image/jpeg', 1, 1614051985, 1622624472, 1622624472);
INSERT INTO `hq_uploads` VALUES (231, 0, 1, 'local', '', 'uploads/20210315/89e33d50735a9e4fd56669b3ba16a0bf.jpg', '29614', 'image/jpeg', 1, 1615737983, 1615738043, 1615738043);
INSERT INTO `hq_uploads` VALUES (232, 0, 1, 'local', '', 'uploads/20210319/fac3acca87cb8ec9523ce13ca35ea3fa.png', '4943', 'image/png', 1, 1616135745, 1617954677, 1617954677);
INSERT INTO `hq_uploads` VALUES (233, 0, 1, 'local', '', 'uploads/20210406/f22fdfc08f39234f59a2e8c710153b62.jpg', '75124', 'image/jpeg', 1, 1617676013, 1617676013, 0);
INSERT INTO `hq_uploads` VALUES (234, 0, 1, 'local', '', 'uploads/20210406/749ff2e6452ca55dfc0913e62cb32bab.jpg', '43422', 'image/jpeg', 1, 1617676063, 1617676063, 0);
INSERT INTO `hq_uploads` VALUES (235, 0, 1, 'local', '', 'uploads/20210421/5f02225c7d94cf534b1d8aac5c0deb57.jpg', '17537', 'image/jpeg', 1, 1619015068, 1619015068, 0);
INSERT INTO `hq_uploads` VALUES (236, 0, 1, 'local', '', 'uploads/20210505/8f2cf8d877cc125b3a27583cda7d2c25.png', '118689', 'image/png', 1, 1620181155, 1620376899, 1620376899);
INSERT INTO `hq_uploads` VALUES (237, 0, 1, 'local', '', 'uploads/20210505/ec793225fa05c278ab929b84bb378356.png', '329088', 'image/png', 1, 1620181174, 1620376897, 1620376897);
INSERT INTO `hq_uploads` VALUES (238, 0, 1, 'local', '', 'uploads/20210505/9eef852c97d855b7aafb8d50a5ea3772.png', '329088', 'image/png', 1, 1620181952, 1620376894, 1620376894);
INSERT INTO `hq_uploads` VALUES (239, 0, 1, 'local', '', 'uploads/20210507/8189c56f73254ef9ab2969c439bf336d.png', '144127', 'image/png', 1, 1620376495, 1620376891, 1620376891);
INSERT INTO `hq_uploads` VALUES (240, 0, 1, 'local', '', 'uploads/20210507/cfaa1396e29781f996248f9b3072a016.png', '164982', 'image/png', 1, 1620376517, 1620376888, 1620376888);
INSERT INTO `hq_uploads` VALUES (241, 0, 1, 'local', '', 'uploads/20210507/5f0ef43cfb084601ad4122777dc6c99d.png', '144463', 'image/png', 1, 1620376629, 1620376885, 1620376885);
INSERT INTO `hq_uploads` VALUES (242, 0, 1, 'local', '', 'uploads/20210507/ddb387e87de278d3cb3e8d7a5ecc75e9.png', '2156203', 'image/png', 1, 1620376870, 1620376883, 1620376883);
INSERT INTO `hq_uploads` VALUES (243, 0, 1, 'local', '', 'uploads/20210507/5c2036313f3f7c7bba96fc2c2b900734.png', '2156203', 'image/png', 1, 1620377210, 1620377219, 1620377219);
INSERT INTO `hq_uploads` VALUES (244, 0, 1, 'local', '', 'uploads/20210508/7f2aaf4f09c0d9e7f640fe2f085b9bbf.mp4', '940673', 'video/mp4', 1, 1620467375, 1620467406, 1620467406);
INSERT INTO `hq_uploads` VALUES (245, 0, 1, 'local', '', 'uploads/20210508/316a308bbac8ef2f9d0d2b1e7b60144e.mp4', '940673', 'video/mp4', 1, 1620467450, 1622624477, 1622624477);
INSERT INTO `hq_uploads` VALUES (246, 0, 1, 'local', '', 'uploads/20210516/3a2f65edcf0c70a1fd44cb056e00832a.png', '8007', 'image/png', 1, 1621138837, 1621138837, 0);
INSERT INTO `hq_uploads` VALUES (247, 0, 1, 'local', '', 'uploads/20210516/b3ba150245d964acca180b7aea133ee6.png', '8007', 'image/png', 1, 1621156358, 1621156358, 0);
INSERT INTO `hq_uploads` VALUES (248, 0, 1, 'local', '', 'uploads/20210602/bbfc36828d9bee2dc584c2085b71b0f4.png', '197460', 'image/png', 1, 1622618613, 1622624247, 1622624247);
INSERT INTO `hq_uploads` VALUES (249, 0, 1, 'local', '', 'uploads/20210602/31766cdfba97ab4b45b5518007b67963.jpg', '704978', 'image/jpeg', 1, 1622623527, 1622624244, 1622624244);
INSERT INTO `hq_uploads` VALUES (250, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/165039_616895_793.jpg', '141168', 'image/jpeg', 1, 1622623839, 1622624228, 1622624228);
INSERT INTO `hq_uploads` VALUES (251, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/165242_827180_222.jpg', '11033', 'image/jpeg', 1, 1622623962, 1622624192, 1622624192);
INSERT INTO `hq_uploads` VALUES (252, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/172146_252238_805.jpg', '11784', 'image/jpeg', 1, 1622625706, 1622625706, 0);
INSERT INTO `hq_uploads` VALUES (253, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/172200_631490_56.jpg', '11784', 'image/jpeg', 1, 1622625720, 1622625720, 0);
INSERT INTO `hq_uploads` VALUES (254, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/172233_967674_408.jpg', '11784', 'image/jpeg', 1, 1622625754, 1622625754, 0);
INSERT INTO `hq_uploads` VALUES (255, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/172251_028836_76.jpg', '11784', 'image/jpeg', 1, 1622625771, 1622625771, 0);
INSERT INTO `hq_uploads` VALUES (256, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/172425_436764_861.jpg', '11784', 'image/jpeg', 1, 1622625865, 1622625865, 0);
INSERT INTO `hq_uploads` VALUES (257, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/172447_921643_450.jpg', '11784', 'image/jpeg', 1, 1622625888, 1622625888, 0);
INSERT INTO `hq_uploads` VALUES (258, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/172714_523837_333.jpg', '11784', 'image/jpeg', 1, 1622626034, 1622626034, 0);
INSERT INTO `hq_uploads` VALUES (259, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/173107_542103_592.jpg', '11784', 'image/jpeg', 1, 1622626267, 1622626267, 0);
INSERT INTO `hq_uploads` VALUES (260, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/173135_766463_637.jpg', '11784', 'image/jpeg', 1, 1622626295, 1622626295, 0);
INSERT INTO `hq_uploads` VALUES (261, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/173148_514548_749.jpg', '11784', 'image/jpeg', 1, 1622626308, 1622626308, 0);
INSERT INTO `hq_uploads` VALUES (262, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/173506_005888_493.jpg', '11784', 'image/jpeg', 1, 1622626506, 1622626506, 0);
INSERT INTO `hq_uploads` VALUES (263, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/173520_752410_617.jpg', '11784', 'image/jpeg', 1, 1622626520, 1622626520, 0);
INSERT INTO `hq_uploads` VALUES (264, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/173731_621828_908.jpg', '11784', 'image/jpeg', 1, 1622626651, 1622626651, 0);
INSERT INTO `hq_uploads` VALUES (265, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/173836_068459_990.jpg', '11784', 'image/jpeg', 1, 1622626716, 1622626716, 0);
INSERT INTO `hq_uploads` VALUES (266, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/173915_637630_349.jpg', '11784', 'image/jpeg', 1, 1622626755, 1622626755, 0);
INSERT INTO `hq_uploads` VALUES (267, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/173928_103416_268.jpg', '11784', 'image/jpeg', 1, 1622626768, 1622626768, 0);
INSERT INTO `hq_uploads` VALUES (268, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/173931_753563_324.jpg', '11033', 'image/jpeg', 1, 1622626771, 1622626771, 0);
INSERT INTO `hq_uploads` VALUES (269, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/205705_766663_381.jpg', '11784', 'image/jpeg', 1, 1622638625, 1622638625, 0);
INSERT INTO `hq_uploads` VALUES (270, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/205727_805766_892.jpg', '11784', 'image/jpeg', 1, 1622638647, 1622638647, 0);
INSERT INTO `hq_uploads` VALUES (271, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/205838_168197_74.jpg', '11784', 'image/jpeg', 1, 1622638718, 1622638718, 0);
INSERT INTO `hq_uploads` VALUES (272, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/205926_698586_762.jpg', '11784', 'image/jpeg', 1, 1622638766, 1622638766, 0);
INSERT INTO `hq_uploads` VALUES (273, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/205952_221883_271.jpg', '11784', 'image/jpeg', 1, 1622638792, 1622638792, 0);
INSERT INTO `hq_uploads` VALUES (274, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/210010_908548_708.jpg', '11784', 'image/jpeg', 1, 1622638811, 1622638811, 0);
INSERT INTO `hq_uploads` VALUES (275, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/210048_608751_224.jpg', '11784', 'image/jpeg', 1, 1622638848, 1622638848, 0);
INSERT INTO `hq_uploads` VALUES (276, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/210246_738405_993.jpg', '11784', 'image/jpeg', 1, 1622638966, 1622638966, 0);
INSERT INTO `hq_uploads` VALUES (277, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/210303_808029_417.jpg', '11784', 'image/jpeg', 1, 1622638983, 1622638983, 0);
INSERT INTO `hq_uploads` VALUES (278, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/210338_650962_99.jpg', '11784', 'image/jpeg', 1, 1622639018, 1622639018, 0);
INSERT INTO `hq_uploads` VALUES (279, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/210342_314019_522.jpg', '5331', 'image/jpeg', 1, 1622639022, 1622639022, 0);
INSERT INTO `hq_uploads` VALUES (280, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/210410_877495_980.jpg', '11784', 'image/jpeg', 1, 1622639050, 1622639050, 0);
INSERT INTO `hq_uploads` VALUES (281, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/210415_465646_803.jpg', '5331', 'image/jpeg', 1, 1622639055, 1622639055, 0);
INSERT INTO `hq_uploads` VALUES (282, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/210446_542844_863.jpg', '11784', 'image/jpeg', 1, 1622639086, 1622639086, 0);
INSERT INTO `hq_uploads` VALUES (283, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/210519_354855_506.jpg', '11784', 'image/jpeg', 1, 1622639119, 1622639119, 0);
INSERT INTO `hq_uploads` VALUES (284, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/210523_418643_327.jpg', '5331', 'image/jpeg', 1, 1622639123, 1622639123, 0);
INSERT INTO `hq_uploads` VALUES (285, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/211416_004398_987.png', '7765', 'image/png', 1, 1622639656, 1622639656, 0);
INSERT INTO `hq_uploads` VALUES (286, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/212412_982544_142.jpg', '11784', 'image/jpeg', 1, 1622640253, 1622640253, 0);
INSERT INTO `hq_uploads` VALUES (287, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/212440_713548_35.jpg', '11784', 'image/jpeg', 1, 1622640280, 1622640280, 0);
INSERT INTO `hq_uploads` VALUES (288, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/212542_810152_361.jpg', '11784', 'image/jpeg', 1, 1622640343, 1622640343, 0);
INSERT INTO `hq_uploads` VALUES (289, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/212636_181105_643.jpg', '11784', 'image/jpeg', 1, 1622640396, 1622640396, 0);
INSERT INTO `hq_uploads` VALUES (290, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/212643_589961_944.jpg', '11033', 'image/jpeg', 1, 1622640403, 1622640403, 0);
INSERT INTO `hq_uploads` VALUES (291, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/213709_268211_153.jpg', '11784', 'image/jpeg', 1, 1622641029, 1622641029, 0);
INSERT INTO `hq_uploads` VALUES (292, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/213804_226562_291.jpg', '11784', 'image/jpeg', 1, 1622641084, 1622641084, 0);
INSERT INTO `hq_uploads` VALUES (293, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/214407_550163_849.jpg', '11784', 'image/jpeg', 1, 1622641447, 1622641447, 0);
INSERT INTO `hq_uploads` VALUES (294, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/214646_724385_860.jpg', '11033', 'image/jpeg', 1, 1622641606, 1622641606, 0);
INSERT INTO `hq_uploads` VALUES (295, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/214724_700231_37.jpg', '11784', 'image/jpeg', 1, 1622641644, 1622641644, 0);
INSERT INTO `hq_uploads` VALUES (296, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/214748_683061_988.png', '16745', 'image/png', 1, 1622641668, 1622641668, 0);
INSERT INTO `hq_uploads` VALUES (297, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/214800_310743_805.png', '16745', 'image/png', 1, 1622641680, 1622641680, 0);
INSERT INTO `hq_uploads` VALUES (298, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/214907_082110_204.png', '16745', 'image/png', 1, 1622641747, 1622641747, 0);
INSERT INTO `hq_uploads` VALUES (299, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/215035_842660_805.png', '16745', 'image/png', 1, 1622641835, 1622641835, 0);
INSERT INTO `hq_uploads` VALUES (300, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/215524_877516_831.jpg', '3457', 'image/jpeg', 1, 1622642124, 1622642124, 0);
INSERT INTO `hq_uploads` VALUES (301, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/215919_619193_910.jpg', '3457', 'image/jpeg', 1, 1622642359, 1622642359, 0);
INSERT INTO `hq_uploads` VALUES (302, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/220025_296656_963.png', '16745', 'image/png', 1, 1622642425, 1622642425, 0);
INSERT INTO `hq_uploads` VALUES (303, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/220055_893551_378.png', '24219', 'image/png', 1, 1622642456, 1622642456, 0);
INSERT INTO `hq_uploads` VALUES (304, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/220116_598265_732.png', '24219', 'image/png', 1, 1622642476, 1622642476, 0);
INSERT INTO `hq_uploads` VALUES (305, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/220231_307281_887.jpg', '3457', 'image/jpeg', 1, 1622642551, 1622642551, 0);
INSERT INTO `hq_uploads` VALUES (306, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/220304_430869_82.jpg', '3457', 'image/jpeg', 1, 1622642584, 1622642584, 0);
INSERT INTO `hq_uploads` VALUES (307, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/220354_817102_749.jpg', '2881', 'image/jpeg', 1, 1622642634, 1622642634, 0);
INSERT INTO `hq_uploads` VALUES (308, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/220416_414080_888.jpg', '2881', 'image/jpeg', 1, 1622642656, 1622642656, 0);
INSERT INTO `hq_uploads` VALUES (309, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/220456_554431_545.png', '28361', 'image/png', 1, 1622642696, 1622642696, 0);
INSERT INTO `hq_uploads` VALUES (310, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/220647_459020_921.png', '16745', 'image/png', 1, 1622642807, 1622642807, 0);
INSERT INTO `hq_uploads` VALUES (311, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/220714_983599_645.png', '16745', 'image/png', 1, 1622642835, 1622642835, 0);
INSERT INTO `hq_uploads` VALUES (312, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/220739_392183_537.png', '28361', 'image/png', 1, 1622642859, 1622642859, 0);
INSERT INTO `hq_uploads` VALUES (313, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/220912_460839_749.png', '28361', 'image/png', 1, 1622642952, 1622642952, 0);
INSERT INTO `hq_uploads` VALUES (314, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/220917_496090_606.png', '16745', 'image/png', 1, 1622642957, 1622642957, 0);
INSERT INTO `hq_uploads` VALUES (315, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/220927_369236_521.png', '28361', 'image/png', 1, 1622642967, 1622642967, 0);
INSERT INTO `hq_uploads` VALUES (316, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/220943_009172_847.png', '16745', 'image/png', 1, 1622642983, 1622642983, 0);
INSERT INTO `hq_uploads` VALUES (317, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/221018_189680_430.png', '28361', 'image/png', 1, 1622643018, 1622643018, 0);
INSERT INTO `hq_uploads` VALUES (318, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/221029_217054_202.png', '16745', 'image/png', 1, 1622643029, 1622643029, 0);
INSERT INTO `hq_uploads` VALUES (319, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/221103_900933_90.png', '16745', 'image/png', 1, 1622643064, 1622643064, 0);
INSERT INTO `hq_uploads` VALUES (320, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/221116_699595_50.png', '28361', 'image/png', 1, 1622643076, 1622643076, 0);
INSERT INTO `hq_uploads` VALUES (321, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/221427_452511_550.png', '28361', 'image/png', 1, 1622643267, 1622643267, 0);
INSERT INTO `hq_uploads` VALUES (322, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/221507_361439_966.png', '28361', 'image/png', 1, 1622643307, 1622643307, 0);
INSERT INTO `hq_uploads` VALUES (323, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/221609_682127_10.jpg', '2881', 'image/jpeg', 1, 1622643369, 1622643369, 0);
INSERT INTO `hq_uploads` VALUES (324, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/221853_621555_800.png', '16745', 'image/png', 1, 1622643533, 1622643533, 0);
INSERT INTO `hq_uploads` VALUES (325, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/221909_866633_129.png', '24219', 'image/png', 1, 1622643550, 1622643550, 0);
INSERT INTO `hq_uploads` VALUES (326, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/221923_197623_271.jpg', '3457', 'image/jpeg', 1, 1622643563, 1622643563, 0);
INSERT INTO `hq_uploads` VALUES (327, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/222605_439853_993.jpg', '5331', 'image/jpeg', 1, 1622643965, 1622643965, 0);
INSERT INTO `hq_uploads` VALUES (328, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/222618_390763_333.jpg', '5331', 'image/jpeg', 1, 1622643978, 1622643978, 0);
INSERT INTO `hq_uploads` VALUES (329, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/222659_822845_498.jpg', '11784', 'image/jpeg', 1, 1622644020, 1622644020, 0);
INSERT INTO `hq_uploads` VALUES (330, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/222819_706934_915.jpg', '11033', 'image/jpeg', 1, 1622644099, 1622644099, 0);
INSERT INTO `hq_uploads` VALUES (331, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/223334_066376_948.png', '39458', 'image/png', 1, 1622644414, 1622644414, 0);
INSERT INTO `hq_uploads` VALUES (332, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/223854_292160_751.jpg', '11784', 'image/jpeg', 1, 1622644734, 1622644734, 0);
INSERT INTO `hq_uploads` VALUES (333, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/223906_837874_626.jpg', '11033', 'image/jpeg', 1, 1622644746, 1622644746, 0);
INSERT INTO `hq_uploads` VALUES (334, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/223923_572215_337.jpg', '11784', 'image/jpeg', 1, 1622644763, 1622644763, 0);
INSERT INTO `hq_uploads` VALUES (335, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/223931_325395_834.jpg', '11033', 'image/jpeg', 1, 1622644771, 1622644771, 0);
INSERT INTO `hq_uploads` VALUES (336, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/223942_231673_833.jpg', '11033', 'image/jpeg', 1, 1622644782, 1622644782, 0);
INSERT INTO `hq_uploads` VALUES (337, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/224019_936498_306.png', '16745', 'image/png', 1, 1622644820, 1622644820, 0);
INSERT INTO `hq_uploads` VALUES (338, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/224144_022097_31.png', '28361', 'image/png', 1, 1622644904, 1622644904, 0);
INSERT INTO `hq_uploads` VALUES (339, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/224210_455357_907.png', '16745', 'image/png', 1, 1622644930, 1622644930, 0);
INSERT INTO `hq_uploads` VALUES (340, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/224320_350836_396.png', '16745', 'image/png', 1, 1622645000, 1622645000, 0);
INSERT INTO `hq_uploads` VALUES (341, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/224336_716132_493.png', '28361', 'image/png', 1, 1622645016, 1622645016, 0);
INSERT INTO `hq_uploads` VALUES (342, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/224404_892496_553.png', '28361', 'image/png', 1, 1622645044, 1622645044, 0);
INSERT INTO `hq_uploads` VALUES (343, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/224436_002383_735.png', '28361', 'image/png', 1, 1622645076, 1622645076, 0);
INSERT INTO `hq_uploads` VALUES (344, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/224507_740290_617.jpg', '7028', 'image/jpeg', 1, 1622645107, 1622645107, 0);
INSERT INTO `hq_uploads` VALUES (345, 0, 1, 'qiniu', 'http://mztyg.ennn.cn/', '2021/0602/224829_912780_134.png', '16745', 'image/png', 1, 1622645310, 1622645310, 0);
INSERT INTO `hq_uploads` VALUES (346, 0, 1, 'local', '', 'uploads/20210602/40b6fee22f5cd6503904dfcbc645236e.jpg', '3457', 'image/jpeg', 1, 1622645455, 1622645455, 0);
INSERT INTO `hq_uploads` VALUES (347, 0, 1, 'local', '', 'uploads/20210602/33584ffe96cb1e52794967e675557fcb.jpg', '562407', 'image/jpeg', 1, 1622645478, 1622645478, 0);

-- ----------------------------
-- Table structure for hq_user
-- ----------------------------
DROP TABLE IF EXISTS `hq_user`;
CREATE TABLE `hq_user`  (
  `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '姓名 ',
  `mobile` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机号码',
  `nickname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '昵称',
  `avatarurl` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '头像',
  `gender` tinyint(3) NULL DEFAULT NULL COMMENT '性别',
  `province` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '省',
  `city` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '市',
  `region` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '区县',
  `openid` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'OPENID',
  `is_customer` tinyint(2) NOT NULL DEFAULT 1 COMMENT '是否客户 0无效用户 1普通用户 2客户',
  `is_agent` tinyint(2) NOT NULL DEFAULT 0 COMMENT '是否代理商 0否 1普通代理商  2商家代理商',
  `agent_type` smallint(5) NOT NULL DEFAULT 0 COMMENT '代理商标签',
  `is_deal` tinyint(2) NOT NULL DEFAULT 0 COMMENT '是否成交 0否 1是',
  `status` tinyint(5) NULL DEFAULT 1 COMMENT '状态 0禁用 1启用',
  `balance` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '账户余额',
  `sort_order` int(5) NOT NULL DEFAULT 100 COMMENT '排序',
  `tags` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '客户标签',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '客户备注',
  `show_coupon_time` int(11) NOT NULL DEFAULT 0 COMMENT '优惠券弹窗时间',
  `service_id` int(5) NOT NULL DEFAULT 0 COMMENT '客服ID',
  `unread_service` mediumint(10) NOT NULL DEFAULT 0 COMMENT '客服未读',
  `unread_relation` mediumint(10) NOT NULL DEFAULT 0 COMMENT '代理商未读',
  `create_time` int(11) NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(11) NOT NULL DEFAULT 0 COMMENT '更新时间',
  `delete_time` int(11) NOT NULL DEFAULT 0 COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 41 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of hq_user
-- ----------------------------
INSERT INTO `hq_user` VALUES (34, '1111111', '1111111111111111', NULL, NULL, NULL, '北京', '北京市', '西城区', NULL, 1, 0, 0, 0, 1, 0.00, 100, '牛逼客户', '111', 0, 0, 0, 0, 1610869206, 1610869206, 0);
INSERT INTO `hq_user` VALUES (35, '1121333', '1234567', NULL, NULL, NULL, '天津', '天津市', '河西区', NULL, 1, 0, 0, 0, 1, 0.00, 100, '', '', 0, 0, 0, 0, 1610869231, 1616261573, 0);
INSERT INTO `hq_user` VALUES (36, '', '', NULL, NULL, NULL, '', '', '', NULL, 1, 0, 0, 0, 1, 0.00, 100, '', '', 0, 0, 0, 0, 1613478269, 1613478269, 0);
INSERT INTO `hq_user` VALUES (37, '', '', NULL, NULL, NULL, '', '', '', NULL, 1, 0, 0, 0, 1, 0.00, 100, '', '', 0, 0, 0, 0, 1613478323, 1614821338, 0);
INSERT INTO `hq_user` VALUES (38, '1', '1234567', NULL, NULL, NULL, '', '', '', NULL, 1, 0, 0, 0, 1, 0.00, 100, '', '', 0, 0, 0, 0, 1617555616, 1617555616, 0);
INSERT INTO `hq_user` VALUES (39, '+33999999', '33999999', NULL, NULL, NULL, '', '', '', NULL, 1, 0, 0, 0, 1, 0.00, 100, '', '', 0, 0, 0, 0, 1617555664, 1617555664, 0);
INSERT INTO `hq_user` VALUES (40, '+33999999', '33999999', NULL, NULL, NULL, '', '', '', NULL, 1, 0, 0, 0, 1, 0.00, 100, '', '', 0, 0, 0, 0, 1617555678, 1619153994, 0);

-- ----------------------------
-- Table structure for hq_zhuanti
-- ----------------------------
DROP TABLE IF EXISTS `hq_zhuanti`;
CREATE TABLE `hq_zhuanti`  (
  `id` int(8) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_id` int(5) NOT NULL DEFAULT 0 COMMENT 'ID',
  `cid` int(5) NOT NULL COMMENT '分类ID',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题',
  `tags` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标签',
  `keywords` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '关键字',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '封面',
  `view` int(10) NOT NULL DEFAULT 0 COMMENT '阅读量',
  `comment` int(10) NOT NULL DEFAULT 0 COMMENT '评论数',
  `like` int(10) NOT NULL DEFAULT 0 COMMENT '点赞数',
  `favor` int(10) NOT NULL DEFAULT 0 COMMENT '收藏数',
  `content` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '详细内容',
  `sort_order` tinyint(5) NOT NULL DEFAULT 100 COMMENT '排序',
  `status` tinyint(5) NOT NULL DEFAULT 0 COMMENT '状态 0待审核 1审核通过 2审核拒绝',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'URL',
  `wxapp_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '小程序链接',
  `is_hot` tinyint(3) NOT NULL DEFAULT 0 COMMENT '是否热门 0不热门 1热门',
  `is_tuijian` tinyint(3) NOT NULL DEFAULT 0 COMMENT '是否推荐 0不推荐 1推荐',
  `create_time` int(11) NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(11) NOT NULL DEFAULT 0 COMMENT '更新时间',
  `delete_time` int(11) NOT NULL DEFAULT 0 COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 130 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '专题表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of hq_zhuanti
-- ----------------------------
INSERT INTO `hq_zhuanti` VALUES (128, 0, 54, '这是一篇文章', '是的啊,是打发萨顶顶,是发送', '是打发', '沙发是打发是打发啊是打发阿什顿发阿什顿发是打发啥都发是打发啥都发', 'https://mdf.muxue.com.cn/uploads/20200811/19019ae3e698cb4dd052ea0fe7d3ea78.png', 6, 0, 0, 0, '<p><img src=\"https://mdf.muxue.com.cn/uploads/20200811/b3e76c813ac225e526de74f891265f04.png\" /></p>\r\n<p>啊是打发爱上大是打发啊少的地方了王嘉尔;发市场艾维尔</p>\r\n<p>发阿什顿啊市场问查完人才市场微创</p>', 100, 1, '/show/128.html', '/pages/list/article?id=128', 0, 0, 1597126342, 1597209144, 1597209144);
INSERT INTO `hq_zhuanti` VALUES (129, 0, 54, '普通的专题', '', '啥是打发啥都,地方,啊', '是的发烧地方阿什顿发是打发', 'https://mdf.muxue.com.cn/uploads/20200812/381c1e3f0f8750de0094d67edb2e61ae.jpg', 67, 0, 0, 0, '<p><img src=\"https://mdf.muxue.com.cn/uploads/20200812/09ad64a48ae491366337fa493f42612f.jpg\" alt=\"\" width=\"600\" height=\"4735\" /></p>', 100, 1, '/show/129.html', '/pages/zhuanti/article?id=129', 0, 0, 1597126376, 1597221223, 0);

SET FOREIGN_KEY_CHECKS = 1;
