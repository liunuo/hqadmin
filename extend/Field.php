<?php

class Field
{
    /**
     * 单图上传
     * @param $id
     * @param $name
     * @param $value
     * @return string
     */
    static function image($value,$id,$name)
    {
        if(empty($value)){
            $hide = "layui-hide";
            $show = "";
        }else{
            $hide = "";
            $show = "layui-hide";
        }
        $ret = <<<EOT
            <style>
                .upload-images-css{padding: 5px; position: relative;}
            </style>
            <div class="layui-upload-drag upload-images-css">
                <div class="ajax-admin-images" id="{$id}" >
                    <div class="{$show} upload-name">
                        <i class="layui-icon sm"></i>
                        <p>点击或将文件拖拽到此处</p>
                    </div>
                    <div class="{$hide} upload-image">
                        <img src="{$value}" alt="上传成功后渲染" style="max-width: 179px">
                        <input type="hidden" name="{$name}" value="{$value}" autocomplete="off" placeholder="" class="layui-input">
                    </div>
                    <div class="layui-progress layui-progress-big upload-progress layui-hide" lay-showpercent="yes" lay-filter="{$id}">
                        <div class="layui-progress-bar" lay-percent=""></div>
                    </div>
                </div>
                <a id="removeImgage" class="layui-btn layui-btn-sm layui-btn-danger layui-btn-position {$hide}" style="margin:5px;" data-id="{$id}"><i class="layui-icon layui-icon-delete" style="color:#ffffff"></i></a>
            </div>
        EOT;
        return $ret;
    }

    /**
     * 多图上传
     * @param $value
     * @return string
     */
    static function photo($value)
    {
        $data = '';
        if(isset($value) && !empty($value)){
            foreach ($value as $k=>$v) {
                $data .= "<p class=\"photosList\"><img src=\"$v\" class=\"layui-upload-img\"><a id=\"removeImg\" class=\"layui-btn layui-btn-sm layui-btn-danger\"><i class=\"layui-icon\">&#xe640;</i>删除</a><input type=\"hidden\" name=\"photo[]\" value=\"$v\"/></p>";
            }
        }
        $ret = <<<EOT
            <button type="button" class="layui-btn layui-btn-normal ajax-admin-photos">上传图片</button>
            <input type="hidden" name="photo[]"/>
            <blockquote class="layui-elem-quote layui-quote-nm" style="margin-top: 10px;">
                <p>图片大小控制在2M以内（可拖拽图片调整显示顺序）</p>
                <div class="layui-upload-list" id="photosView">
                    {$data}
                </div>
            </blockquote>
            <script type="text/javascript" src="/static/hqui/js/sortable.min.js"></script>
            <script>
            // 图片拖拽排序
                var photosView = document.getElementById('photosView');
                new Sortable(photosView, {
                    animation: 200,
                });
            </script>
        EOT;
        return $ret;
    }

    /**
     * 表单-标签
     * @param $id
     * @param $name
     * @param $value
     * @return string
     */
    static function tags($value,$id,$name)
    {
        if(empty($value)){
            $hide = "layui-hide";
            $show = "";
        }else{
            $hide = "";
            $show = "layui-hide";
        }
        $ret = <<<EOT
            <div class="ajax-tags" data-id="{$id}">
                <input id="{$id}" name="{$name}" value="{$value}" class="layui-input"/>
            </div>
        EOT;
        return $ret;
    }

    /**
     * 文件上传
     * @param $value
     * @return string
     */
    static function file($value)
    {
        $data = '';
        if(isset($value) && !empty($value)){
            foreach ($value as $k=>$v) {
                $data .= "
                    <tr id=\"upload-$k\">
                        <td><i class=\"layui-icon layui-icon-face-smile layui-icon-file\"></i> $v<input type=\"hidden\" name=\"file[]\" value=\"$v\"/></td>
                        <td></td>
                        <td><span class=\"text-success\">已上传</span></td>
                        <td>
                            <a href=\"$v\" target=\"_blank\" class=\"layui-btn layui-btn-xs layui-btn-normal\">下载</a>
                            <a class=\"layui-btn layui-btn-xs layui-btn-danger file-delete\">删除</a>
                        </td>
                    </tr>";
            }
        }
        $ret = <<<EOT
            <div class="layui-upload">
                <button type="button" class="layui-btn layui-btn-normal ajax-admin-file">选择文件</button>
                <div class="layui-upload-list" style="max-width: 1000px;">
                    <table class="layui-table">
                        <colgroup>
                            <col>
                            <col width="150">
                            <col width="260">
                            <col width="150">
                        </colgroup>
                        <thead>
                        <tr><th>文件名</th>
                            <th>大小</th>
                            <th>上传进度</th>
                            <th>操作</th>
                        </tr></thead>
                        <tbody id="file-lists">
                            {$data}
                        </tbody>
                    </table>
                    <script>
                        
                    </script>
                </div>
            </div>
        EOT;
        return $ret;
    }



}