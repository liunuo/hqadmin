layui.use(['jquery'], function () {
    var $ = layui.jquery;
    var upload_url = '/admin/uploads/editor_upload';
    // 渲染富文本编辑器
    tinymce.init({
        selector: '#demoEditor',
        height: 525,
        branding: false,
        language: 'zh_CN',
        plugins: 'code print preview fullscreen paste searchreplace save autosave link autolink image imagetools media table codesample lists advlist hr charmap emoticons anchor directionality pagebreak quickbars nonbreaking visualblocks visualchars wordcount',
        toolbar: 'fullscreen preview code | undo redo | forecolor backcolor | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | outdent indent | numlist bullist | formatselect fontselect fontsizeselect | link image media emoticons charmap anchor pagebreak codesample | ltr rtl',
        toolbar_drawer: 'sliding',
        image_advtab: true, //开启图片上传的高级选项功能
        relative_urls : false,
        remove_script_host : false,
        table_default_styles: {
            width: '100%',
             borderCollapse: 'collapse'
        },
        image_title: false, // 是否开启图片标题设置的选择，这里设置否
        automatic_uploads: true, //开启点击图片上传时，自动进行远程上传操作
        images_upload_handler: (blobInfo, success, failure) => { // 图片异步上传处理函数
            var xhr, formData;
            xhr = new XMLHttpRequest();
            xhr.withCredentials = false;
            xhr.open('POST', upload_url);
            xhr.onload = function () {
                var json;
                if (xhr.status !== 200) {
                    failure('HTTP Error: ' + xhr.status);
                    return;
                }
                json = JSON.parse(xhr.responseText);
                //console.log(json);
                if (!json || typeof json.location !== 'string') {
                failure('Invalid JSON: ' + xhr.responseText);
                    return;
                }
                success(json.location);
            };

            formData = new FormData();
            formData.append('file', blobInfo.blob(), blobInfo.filename());
            xhr.send(formData);
        },
        images_upload_url: upload_url,
        file_picker_types: 'media',
        file_picker_callback: function (callback, value, meta) {
            //文件分类
            var filetype = '.pdf, .txt, .zip, .rar, .7z, .doc, .docx, .xls, .xlsx, .ppt, .pptx, .mp3, .mp4';
            //后端接收上传文件的地址
            var upurl = upload_url;
            //模拟出一个input用于添加本地文件
            var input = document.createElement('input');
                input.setAttribute('type', 'file');
                input.setAttribute('accept', filetype);
            input.click();
            input.onchange = function() {
                var file = this.files[0];

                var xhr, formData;
                console.log(file.name);
                xhr = new XMLHttpRequest();
                xhr.withCredentials = false;
                xhr.open('POST', upurl);
                xhr.onload = function() {
                    var json;
                    if (xhr.status != 200) {
                        failure('HTTP Error: ' + xhr.status);
                        return;
                    }
                    json = JSON.parse(xhr.responseText);
                    if (!json || typeof json.location != 'string') {
                        failure('Invalid JSON: ' + xhr.responseText);
                        return;
                    }
                    callback(json.location);
                };
                
                formData = new FormData();
                formData.append('file', file, file.name );
                xhr.send(formData);
                console.log(xhr);
            }
        },
        setup: function(editor){ 
            editor.on('change',function(){ editor.save(); });
        }
    });

});